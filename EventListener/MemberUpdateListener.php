<?php

namespace La\SmartFocusMembersBundle\EventListener;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use La\SmartFocusMembersBundle\Event\MemberUpdateEvent;

class MemberUpdateListener implements EventSubscriberInterface
{
    /** @var Container */
    protected $container;
    /** @var ClientInterface $client */
    protected $client;
    /** @var LoggerInterface $logger */
    protected $logger;

    /**
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     */
    public function __construct(ClientInterface $client, LoggerInterface $logger = null)
    {
            $this->client = $client;
            $this->logger = $logger ?: new NullLogger() ;
    }

    public static function getSubscribedEvents()
    {
        return array(
            MemberUpdateEvent::SMARTFOCUS_MEMBER_UPDATE => array('onMemberUpdateRequest')
        );
    }

    public function onMemberUpdateRequest(MemberUpdateEvent $event)
    {
        try {
            $member = $event->getMember();

//           /*DEBUG*/ $member['ID_CLIENT'] = 10051440;
            if (isset($member['ID_CLIENT']) && $member['ID_CLIENT'] != null) {
                $criteria = array('ID_CLIENT' => $member['ID_CLIENT']);
            } else if (isset($member['EMAIL']) && $member['EMAIL'] != null) {
                $criteria = array('EMAIL' => $member['EMAIL']);
            } else {
                throw new \Exception('Unable to find a a valid identifier for member: ' . serialize($member));
            }
            $return = $this->client->insertOrUpdateMember($criteria, $member);
            $this->logger->info('Successfully sent a member update request: Job Id #' . $return);
        } catch (\Exception $e) {
            $this->logger->error('Unable to process member update request: ' . $e->getMessage());
        }
    }

}
