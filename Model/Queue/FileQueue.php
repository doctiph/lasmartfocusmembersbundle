<?php
namespace La\SmartFocusMembersBundle\Model\Queue;

use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use La\SmartFocusMembersBundle\Model\Provider\MemberProviderInterface;

/**
 * Class FileQueue
 * @package La\SmartFocusMembersBundle\Model\Queue
 */
class FileQueue implements QueueInterface
{
    const QUEUE_SEPARATOR = PHP_EOL;
    const QUEUE_FILENAME = 'export.queue';

    /** @var string */
    protected $queueFile;
    /** @var string */
    protected $queueDir;

    /**
     * @param $queueDir
     */
    public function __construct($queueDir)
    {
        $this->queueDir = $queueDir;
        $this->queueFile = sprintf('%s/%s', $this->queueDir, static::QUEUE_FILENAME);
    }

    /**
     * @inheritdoc
     */
    public function queue($file, $providerAlias)
    {
        // Check if file exists ( $file or $queueDir/$file )
        if (!file_exists($file)) {
            $file = $this->queueDir . '/' . $file;
            if (!file_exists($file)) {
                throw new \Exception('File ' . $file . ' does not exist.');
            }
        }
        $queuedValue = sprintf('%s%s',
            $this->count() ? static::QUEUE_SEPARATOR : '',
            @serialize(array('file' => $file, 'provider' => $providerAlias))
        );
        file_put_contents($this->queueFile, $queuedValue, FILE_APPEND | LOCK_EX);
    }

    /**
     * @inheritdoc
     */
    public function updateQueue($queue)
    {
        file_put_contents($this->queueFile, $queue, LOCK_EX);
    }

    /**
     * @inheritdoc
     */
    public function eraseQueue()
    {
        file_put_contents($this->queueFile, '', LOCK_EX);
    }


    /**
     * @inheritdoc
     */
    public function count()
    {
        $fullQueue = @file_get_contents($this->queueFile);
        if (!$fullQueue) {
            return 0;
        }
        $arrayQueue = explode(static::QUEUE_SEPARATOR, $fullQueue);
        return count($arrayQueue);
    }

    /**
     * @inheritdoc
     */
    public function consume(ClientInterface $client)
    {
        $fullQueue = file_get_contents($this->queueFile);
        $arrayQueue = explode(static::QUEUE_SEPARATOR, $fullQueue);
        $consumable = @array_shift($arrayQueue);
        if ($consumable != null) {
            if ($this->getAvailableUploadSlots($client) > 0) {
                $data = @unserialize($consumable);
                if (is_array($data) && isset($data['file']) && isset($data['provider'])) {
                    $this->updateQueue(implode(static::QUEUE_SEPARATOR, $arrayQueue));
                    return $data;
                } else {
                    throw new \Exception('Corrupted queue. Could not process the value: ' . $consumable);
                }
            } else {
                throw new \Exception('No available upload slot.');
            }
        } else {
            throw new \Exception('No file found in queue.');
        }
    }

    /**
     * @inheritdoc
     */
    public function getAvailableUploadSlots(ClientInterface $client)
    {
        return $client->getImportQueueStatus();
    }


    /**
     * @param null $dir
     * @return array
     */
    public function getQueueableFiles($dir = null)
    {
        if (is_null($dir) || !is_dir($dir)) {
            $dir = $this->queueDir;
        }

        $files = scandir($dir);
        $ignoredFiles = array('.', '..', static::QUEUE_FILENAME);

        $queuableFiles = [];
        foreach ($files as $file) {
            // ignore tmp files ending with ~ or configured ignored files
            if (substr($file, -1) === '~' || in_array($file, $ignoredFiles)) {
                continue;
            }
            $queuableFiles[] = $file;
        }

        return $queuableFiles;
    }


}