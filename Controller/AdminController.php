<?php

namespace La\SmartFocusMembersBundle\Controller;

use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use La\AdminBundle\Controller\AdminController as BaseAdminController;

/**
 * Admin Controller for SmartFocus Member API
 */
class AdminController extends BaseAdminController
{

    protected $currentMenu = 'la_smart_focus_members_admin.nav.stats.default';

    public function historyAction(Request $request)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }

        $data = [];

        /** @var ClientInterface $client */
        $client = $this->get('la_smart_focus_members.client.rest');
        try {
            $lastUploads = $client->getLastUploads();
            if (is_array($data)) {
                foreach ($lastUploads as $upload) {
                    try {
                        $id = $upload['id'];

                        $data[] = array(
                            'id' => $id,
                            'source' => $upload['source'],
                            'shortStatus' => $upload['status'],
                            'status' => $client->getUploadStatus($id),
                        );
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        } catch (\Exception $e) {
            $data = null;
        }

        return $this->render('LaSmartFocusMembersBundle:Admin:Batch/history.html.twig', array(
            'data' => $data,
            'current_menu' => 'la_smart_focus_members_admin.nav.default',
            'current_sub_menu' => 'la_smart_focus_members_admin.nav.history',
        ));
    }

    public function detailAction(Request $request, $id)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }
        /** @var ClientInterface $client */
        $client = $this->get('la_smart_focus_members.client.rest');

        try {
            $logFile = $client->getLogFile($id);
        } catch (\Exception $e) {
            $logFile = null;
        }
        try {
            $badFile = $client->getBadFile($id);
        } catch (\Exception $e) {
            $badFile = null;
        }
        $data = array('id' => $id, 'log' => $logFile, 'bad' => $badFile);

        return $this->render('LaSmartFocusMembersBundle:Admin:Batch/detail.html.twig', array(
            'data' => $data,
            'current_menu' => 'la_smart_focus_members_admin.nav.default',
            'current_sub_menu' => 'la_smart_focus_members_admin.nav.history',
        ));

    }

    public function searchAction(Request $request)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }

        $form = $this->createFormBuilder()
            ->add('email', 'text', array('label' => 'Email', 'required' => false))
            ->add('id', 'text', array('label' => 'Id', 'required' => false))
            ->add('search', 'submit', array('label' => 'Chercher'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            $email = $data['email'];
            $id = $data['id'];

            /** @var ClientInterface $client */
            $client = $this->get('la_smart_focus_members.client.rest');

            $criteria = [];
            if($email !== null){
                $criteria['EMAIL'] = $email;
            }
            if($id !== null){
                $criteria['ID_CLIENT'] = $email;
            }

            if (empty($criteria)) {
                $this->alert('danger', 'Veuillez renseigner au moins l\'un des champs de recherche.');
                return $this->render('LaSmartFocusMembersBundle:Admin:Individual/search.html.twig', array(
                    'form' => $form->createView(),
                    'current_menu' => 'la_smart_focus_members_admin.nav.search',
                    'current_sub_menu' => 'la_smart_focus_members_admin.nav.search',
                ));
            } else {
                try {
                    $error = null;
                    $member = $client->getMembers($criteria);
                } catch (\Exception $e) {
                    $error = $e->getMessage();
                    $member = null;
                }
            }
            if (is_array($member) && count($member) === 0) {
                $this->alert('danger', 'Aucun membre trouvé correspondant à ces paramètres.');
                return $this->render('LaSmartFocusMembersBundle:Admin:Individual/search.html.twig', array(
                    'form' => $form->createView(),
                    'current_menu' => 'la_smart_focus_members_admin.nav.search',
                    'current_sub_menu' => 'la_smart_focus_members_admin.nav.search',
                    'widget' => $email !== null ? $this->container->getParameter('smartfocus.members.widget.no_result') : null,
                    'email' => $email
                ));
            }
            return $this->render('LaSmartFocusMembersBundle:Admin:Individual/member.html.twig', array(
                'member' => $member,
                'error' => $error,
                'current_menu' => 'la_smart_focus_members_admin.nav.search',
                'current_sub_menu' => 'la_smart_focus_members_admin.nav.search',
                'widget' => $this->container->getParameter('smartfocus.members.widget.search_result')
            ));
        }

        return $this->render('LaSmartFocusMembersBundle:Admin:Individual/search.html.twig', array(
            'form' => $form->createView(),
            'current_menu' => 'la_smart_focus_members_admin.nav.search',
            'current_sub_menu' => 'la_smart_focus_members_admin.nav.search',
        ));

    }

}