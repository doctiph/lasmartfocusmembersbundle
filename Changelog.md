Changelog
=========

### Current changes (develop) -> Will appear in next release

### 2.1.2 (2015-09-12)

* Fix Migration 2.7 && Amelioration Command Formater

### 2.1.1 (2015-09-01)

* Bugfix when no member to export

### 2.1.0 (2015-08-27)

* psr4

### 2.0.0 (2015-06-11)

* 2.7

### 1.0.0 (2015-06-11)

* Version stable
