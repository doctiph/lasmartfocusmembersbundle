# SmartFocus Members API

## Glossary
  
  In this documentation, the following terms will be used with a specific meaning:
  
  - __Member__: A member is the entity used to represent a newsletter subscriber in SmartFocus. A member is identified by an ID and has several CRM fields (email, cellphone, etc.) 
    
  - __Provider__: A provider is a PHP class that will know how to transform a user from your user-management system into a SmartFocus member.  Each provider needs a specific member mapping (See below: _Using a custom member provider_)  
    
  - __Mapping__: A mapping is an array matching each SmartFocus member property with a property or a getter from your application user-management system. (See below: _Using LaUser provider_ and _Using a custom member provider_) 

## Installation

### composer.json

      "require": {
        ....
        "la/smartfocusmembersbundle": "~2.0.0"
      },
      
### Configuration

  To properly configure LaSmartFocusMembersBundle, do the following:
  
  Start by launching the following command: 

    php app/console smartfocus:config:generate
    
  Differents options are available. Choose option __Generate config structure__.  
  The command will ask you different parameters: Fill them in or let them blank (You can manually write them in later).  
    
  When all parameters have been prompted, the command exits. The configuration file is generated, you can modify it or repick the same option in the command if you left some parameters empty.  
    
  Once your configuration file is filled with valid credentials/values, you will need to configure a mapping for the provider you wish to use.
  Before you go any further, please read below either:  
  
  * Using LaUser provider  
  or  
  
  * Using a custom member provider  
    
  When you know which provider you'll use,  launch again the command:
    
    php app/console smartfocus:config:generate
                                               
  This time, choose option __Generate mapping structure (SmartFocus Credentials must be valid)__.  
  This option will retrieve a default member mapping for the provider you wish to use.  
  You'll then have to replace the "null" properties in mapping by the correct properties/methods to match.
  
#### Using LaUser provider

  If your user-management system is LaUserBundle 1.3.9 and higher, the bundle comes with a built-in member provider.  
  This provider alias is __lauser__.  
    
  The LaUser member provider 
  
#### Using a custom member provider

  If you want to use another one, you'll have to create a provider extending the abstract user provider with your own logic.

  Create a service extending La/SmartFocusMembersBundle/Model/Provider/AbstractUserProvider    
  It musts implements the method getMembers($date, $offset, $limit). This method provides an array of members (or an instance of \Iterator)    
  It will also implements any custom getter you need.  
  
  Define in your configuration that you'll be using your service as user provider by changing the 'provider' node value under la_smart_focus_members:  
  
        la_smart_focus_members:  
            ....  
            provider: your_custom.user_provider # (That's your service name)  
  
  Note: Make sure that your user provider is able to handle all the fields you defined in your SmartFocus mapping, or they'll be ignored.  
 
### Configuration Reference
 
     la_smart_focus_members:
         server:               ~ # Required
         login:                ~ # Required
         password:             ~ # Required
         key:                  ~ # Required
         providers:              # Required
             # Prototype
             name:
                 mapping:     [] # Required

## LaSmartFocusMembersBundle Batch Update

