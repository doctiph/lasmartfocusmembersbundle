<?php

namespace La\SmartFocusMembersBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\AdminBundle\Event\AdminNavEvent;

class AdminListener implements EventSubscriberInterface
{

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'admin.nav.links' => array('onAdminNavLinksRequest')
        );
    }

    public function onAdminNavLinksRequest(AdminNavEvent $event)
    {
        $event->addLinks(
            array(
                'name' => 'la_smart_focus_members_admin.nav.default',
                'icon' => 'user',
                'links' => array(
                    array(
                        'name' => 'la_smart_focus_members_admin.nav.history',
                        'url' => $this->router->generate('la_smart_focus_members_admin_batch_history'),
                        'role' => 'ROLE_LA_ADMIN',
                    ),
                    array(
                        'name' => 'la_smart_focus_members_admin.nav.search',
                        'url' => $this->router->generate('la_smart_focus_members_admin_individual_search'),
                        'role' => 'ROLE_LA_ADMIN',
                    ),
//                    array(
//                        'name' => 'la_user_admin.nav.users.delete',
//                        'url' => $this->router->generate('la_user_admin_user_delete'),
//                        'role' => 'ROLE_LA_ADMIN_LA_USER_EDIT',
//                    ),
//                    array(
//                        'name' => 'la_user_admin.nav.users.changeemail',
//                        'url' => $this->router->generate('la_user_admin_user_changeemail'),
//                        'role' => 'ROLE_LA_ADMIN_LA_USER_EDIT',
//                    )
                )
            )
        );
    }


}