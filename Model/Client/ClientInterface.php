<?php

namespace La\SmartFocusMembersBundle\Model\Client;

/**
 * Interface ClientInterface
 * @package La\SmartFocusMembersBundle\Model\Client
 */
interface ClientInterface
{
    /**
     *  Max session token age (in seconds)
     */
    const SMARTFOCUS_SESSION_TOKEN_MAX_AGE = 3600;

    /**
     * Open a SmartFocus session: Provides a session token (stored into client)
     * @param $login
     * @param $password
     * @param $key
     * @return string
     */
    public function open($login, $password, $key);

    /**
     * Close the current SmartFocus session
     * @return bool
     */
    public function close();


    ################ MASS DATA API METHODS ################

    /**
     * Uploads a file containing members and inserts them into SmartFocus member table.
     * @param $file
     * @param array $headers
     * @return mixed
     */
    public function uploadAndInsert($file, array $headers);

    /**
     * Uploads a file containing members and merges them with those in SmartFocus member table
     * @param $file
     * @param array $headers
     * @return mixed
     */
    public function uploadAndMerge($file, array $headers);

    /**
     * Retrieves the last 20 uploads for the SmartFocus account and their statuses.
     * @return mixed
     */
    public function getLastUploads();

    /**
     * Retrieves the number of available upload slots (The maximum number of parallel uploads is 5.)
     * @return mixed
     */
    public function getImportQueueStatus();

    /**
     * Retrieves a list of uploads and their details.
     * @return mixed
     */
    public function getUploadSummaryList();

    /**
     * Retrieves the status of a file upload.
     * @param $uploadId
     * @return mixed
     */
    public function getUploadStatus($uploadId);

    /**
     * Retrieves the log file associated with an upload.
     * @param $uploadId
     * @return mixed
     */
    public function getLogFile($uploadId);

    /**
     * Retrieves the lines of an uploaded file that could not be uploaded due to errors.
     * @param $uploadId
     * @return mixed
     */
    public function getBadFile($uploadId);


    ################ INDIVIDUAL DATA API METHODS ################

    /**
     * Inserts a new member with only an email address (other fields are blank)
     * @param $email
     * @return mixed
     */
    public function insertMemberByEmailAddress($email);

    /**
     * Inserts a new member
     * @param $email
     * @param array $dynContent
     * @return mixed
     */
    public function insertMember($email, array $dynContent); // <email>...@...</email><dynContent><entry><key>...</key><value>...</value></entry>...</dynContent>
    // https://{server}/apimember/services/rest/member/insertMember/{token}

    /**
     * Updates one or multiple field for members identified by one or multiple criteria (FIRSTNAME:John|LASTNAME:Smith|EMAIL:johnsmith@smartfocus.com)
     * @param $memberUID
     * @param array $dynContent
     * @return mixed
     */
    public function updateMember(array $memberUID, array $dynContent);
    // https://{server}/apimember/services/rest/member/updateMember/{token}

    /**
     * Updates a single field for a member identified by its email
     * @param $email
     * @param $field
     * @param $value
     * @return mixed
     */
    public function updateMemberByEmailAddress($email, $field, $value);
    // https://{server}/apimember/services/rest/member/update/{token}/{emailAddress}/{fieldName}/{fieldValue}

    /**
     * Creates or updates one or multiple field for members identified by one or multiple criteria (FIRSTNAME:John|LASTNAME:Smith|EMAIL:johnsmith@smartfocus.com)
     * @param array $memberUID
     * @param array $dynContent
     * @return mixed
     */
    public function insertOrUpdateMember(array $memberUID, array $dynContent);
    //https://{server}/apimember/services/rest/member/insertOrUpdateMember/{token}

    /**
     * Retrieves the job status (i.e. the status of the member insertion or update) using the job ID
     * @param $jobId
     * @return mixed
     */
    public function getMemberJobStatus($jobId);
    //https://{server}/apimember/services/rest/member/getMemberJobStatus/{token}/{synchroId}

    /**
     * Retrieves a list of members who have the given email address
     * @param $email
     * @return mixed
     */
    public function getMembersByEmail($email);
    //https://{server}/apimember/services/rest/member/getMemberByEmail/{token}/{emailAddress}

    /**
     * Retrieves a list of members who have the given cellphone
     * @param $cellphone
     * @return mixed
     */
    public function getMembersByCellphone($cellphone);
    //https://{server}/apimember/services/rest/member/getMemberByCellphone/{token}/{cellphone}

    /**
     * Retrieves a member who has the given ID
     * @param $id
     * @return mixed
     */
    public function getMemberById($id);
    //https://{server}/apimember/services/rest/member/getMemberById/{token}/{memberId}

    /**
     * Retrieves all members page by page. Each page contains 10 members
     * @param $page
     * @return mixed
     */
    public function getListMembersByPage($page);
    //https://{server}/apimember/services/rest/getListMembersByPage/{token}/{page}

    /**
     * Retrieves a list of members who match the given criteria.
     * @param array $memberUID
     * @return mixed
     */
    public function getMembers(array $memberUID); // Field1:Value1|Field2:Value2 ...
    //https://{server}/apimember/services/rest/member/getMembers/{token}

    /**
     * Retrieves the list of fields (i.e. database column names) available in the Member table.
     * @return mixed
     */
    public function descMemberTable();
    //https://{server}/apimember/services/rest/member/descMemberTable/{token}

    /**
     * Unsubscribes the member(s) matching the given email address.
     * @param $email
     * @return mixed
     */
    public function unjoinByEmail($email);
    //https://{server}/apimember/services/rest/member/unjoinByEmail/{token}/{emailAddress}

    /**
     * Unsubscribes the member(s) matching the given cellphone.
     * @param $cellphone
     * @return mixed
     */
    public function unjoinByCellphone($cellphone);
    //https://{server}/apimember/services/rest/member/unjoinByEmail/{token}/{cellphone}

    /**
     * Unsubscribes the member matching the given ID.
     * @param $id
     * @return mixed
     */
    public function unjoinById($id);
    //https://{server}/apimember/services/rest/member/unjoinByMemberId/{token}/{memberId}

    /**
     * Unsubscribes the member(s) matching the given criteria
     * @param array $memberUID
     * @return mixed
     */
    public function unjoinMember(array $memberUID);// Field1:Value1|Field2:Value2 ...
    //https://{server}/apimember/services/rest/member/unjoinMember/{token}

    /**
     * Resubscribes the member(s) matching the given email address.
     * @param $email
     * @return mixed
     */
    public function rejoinByEmail($email);
    //https://{server}/apimember/services/rest/member/rejoinByEmail/{token}/{emailAddress}

    /**
     * Resubscribes the member(s) matching the given cellphone
     * @param $cellphone
     * @return mixed
     */
    public function rejoinByCellphone($cellphone);
    //https://{server}/apimember/services/rest/member/rejoinByCellphone/{token}/{cellphone}


    /**
     * Resubscribes the member matching the given ID.
     * @param $id
     * @return mixed
     */
    public function rejoinById($id);
    //https://{server}/apimember/services/rest/member/rejoinById/{token}/{id}









}