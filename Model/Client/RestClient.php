<?php

namespace La\SmartFocusMembersBundle\Model\Client;

use La\SmartFocusMembersBundle\Model\FileGenerator\CsvFileGenerator;

/**
 * Class RestClient
 * @package La\SmartFocusMembersBundle\Model\Client
 */
class RestClient extends AbstractClient
{
    /**
     * Open a SmartFocus session: Provides a session token
     * @param $login
     * @param $password
     * @param $key
     * @return string
     */
    public function open($login, $password, $key)
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/connect/open?login=%s&password=%s&key=%s",
                $this->server,
                $login,
                $password,
                $key
            );

            $response = $this->cURL($url);

            if (isset($response['result'])) {
                $this->setToken($response['result']);
            } else {
                throw new \Exception(sprintf('No token found in %s. (Error occured on %s)', serialize($response), $url));
            }
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Close the current SmartFocus session
     * @return bool
     */
    public function close()
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/connect/close?token=%s",
                $this->server,
                $this->getToken()
            );

            $this->cURL($url);
            $this->token = null;
            $this->tokenExpires = null;
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    ################ MASS DATA API METHODS ################

    /**
     * Uploads a file containing members and inserts them into SmartFocus member table.
     * WARNING: Should not be used if the contains lines to merge, or they'll be seen as duplicates.
     * @param $file
     * @param $headers
     * @return mixed
     */
    public function uploadAndInsert($file, array $headers)
    {
        try {

            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/insertUpload",
                $this->server,
                $this->getToken()
            );

            $eol = "\r\n";
            $mime_boundary = md5(time());

            $data = '';
            $data .= '--' . $mime_boundary . $eol;
            $data .= 'Content-Disposition: form-data; name="insertUpload"' . $eol;
            $data .= 'Content-Type: text/xml' . $eol . $eol;
            $data .= $this->computeInsertUploadXml($file, $headers) . $eol;
            $data .= '--' . $mime_boundary . $eol;
            $data .= 'Content-Disposition: form-data; name="inputStream"; filename="' . basename($file) . '"' . $eol;
            $data .= 'Content-Type: application/octet-stream' . $eol;
            $data .= 'Content-Transfer-Encoding: base64' . $eol . $eol;
            $data .= $this->getChunk64FileContent($file) . $eol;
            $data .= "--" . $mime_boundary . "--" . $eol . $eol; // finish with two eol's!!

            $params = array('http' => array(
                'method' => 'PUT',
                'header' => 'Content-Type: multipart/form-data; boundary=' . $mime_boundary . $eol,
                'content' => $data
            ));

            $response = $this->streamAndSend($url, $params);

            if (!$response) {
                throw new \Exception(sprintf('Unable to send file. The response returned: %s (Error occured while sending %s to %s', isset($http_response_header[0]) ? $http_response_header[0] : 'Nothing (Response header too long).', $file, $url));
            }

            $arrayResponse = $this->xmlResponseToArray($response, $url);

            if (isset($arrayResponse['result'])) {
                return $arrayResponse['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($arrayResponse), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Uploads a file containing members, inserts and merges them with those in SmartFocus member table
     * @param $file
     * @param array $headers
     * @return mixed
     */
    public function uploadAndMerge($file, array $headers)
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/mergeUpload",
                $this->server,
                $this->getToken()
            );

            $eol = "\r\n";
            $mime_boundary = md5(time());

            $data = '';
            $data .= '--' . $mime_boundary . $eol;
            $data .= 'Content-Disposition: form-data; name="mergeUpload"' . $eol;
            $data .= 'Content-Type: text/xml' . $eol . $eol;
            $data .= $this->computeMergeUploadXml($file, $headers, array('EMAIL')) . $eol;
            $data .= '--' . $mime_boundary . $eol;
            $data .= 'Content-Disposition: form-data; name="inputStream"; filename="' . basename($file) . '"' . $eol;
            $data .= 'Content-Type: application/octet-stream' . $eol;
            $data .= 'Content-Transfer-Encoding: base64' . $eol . $eol;
            $data .= $this->getChunk64FileContent($file) . $eol;
            $data .= "--" . $mime_boundary . "--" . $eol . $eol; // finish with two eol's!!

            $params = array('http' => array(
                'method' => 'PUT',
                'header' => 'Content-Type: multipart/form-data; boundary=' . $mime_boundary . $eol,
                'content' => $data,
            ));

            $response = $this->streamAndSend($url, $params);

            if (!$response) {
                throw new \Exception(sprintf('Unable to send file. The response returned: %s (Error occured while sending %s to %s', isset($http_response_header[0]) ? $http_response_header[0] : 'Nothing (Response header too long).', $file, $url));
            }

            $arrayResponse = $this->xmlResponseToArray($response, $url);

            if (isset($arrayResponse['result'])) {
                return $arrayResponse['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($arrayResponse), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }
    /**
     * Retrieves the last 20 uploads for the SmartFocus account and their statuses.
     * @return mixed
     */
    public function getLastUploads()
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/getLastUpload",
                $this->server,
                $this->getToken()
            );

            $response = $this->cURL($url);
            if (isset($response['lastUploads'])) {
                return $response['lastUploads'];
            }
            return array();
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the number of available upload slots (The maximum number of parallel uploads is 5.)
     * @return mixed
     */
    public function getImportQueueStatus()
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/getImportQueueStatus",
                $this->server,
                $this->getToken()
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No import queue status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves a list of uploads and their details.
     * @param int $page
     * @param int $pageSize
     * @param string $sort
     * @param array $search
     * @return mixed
     * @throws \Exception
     */
    public function getUploadSummaryList($page = 1, $pageSize = 10, $sort = 'date', array $search = array())
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/uploads/%d?pageSize=%d", // &sort=%s:
                $this->server,
                $this->getToken(),
                $page,
                $pageSize
//              ,  $sort
            );

            if (!empty($search)) {
                $exploded = [];
                foreach ($search as $key => $value) {
                    $exploded[] = $key . ':' . $value;
                }
                $url .= '&search=' . implode('|', $exploded);
            }

            $response = $this->cURL($url);

            if (isset($response['lastUploads'])) {
                return $response['lastUploads'];
            }
            return array();
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the status of a file upload.
     * @param $uploadId
     * @return mixed
     */
    public function getUploadStatus($uploadId)
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/%s/getUploadStatus",
                $this->server,
                $this->getToken(),
                $uploadId
            );

            $response = $this->cURL($url);
            if (isset($response['uploadStatus'])) {
                return sprintf('%s%s', $response['uploadStatus']['status'], isset($response['uploadStatus']['details']) ? ': ' . $response['uploadStatus']['details'] : '');
            }
            throw new \Exception(sprintf('No upload status found in %s. (Error occured on %s)', serialize($response), $url));

        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the log file associated with an upload.
     * @param $uploadId
     * @return mixed
     */
    public function getLogFile($uploadId)
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/%s/getLogFile",
                $this->server,
                $this->getToken(),
                $uploadId
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No log file found in %s. (Error occured on %s)', serialize($response), $url));

        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the lines of an uploaded file that could not be uploaded due to errors.
     * @param $uploadId
     * @return mixed
     */
    public function getBadFile($uploadId)
    {
        try {
            $url = sprintf("https://%s/apibatchmember/services/rest/batchmemberservice/%s/batchmember/%s/getBadFile",
                $this->server,
                $this->getToken(),
                $uploadId
            );

            $response = $this->cURL($url);

            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No log file found in %s. (Error occured on %s)', serialize($response), $url));

        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    ################ INDIVIDUAL DATA API METHODS ################

    /**
     * Inserts a new member with only an email address (other fields are blank)
     * @param $email
     * @return mixed
     */
    public function insertMemberByEmailAddress($email)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/insert/%s/%s",
                $this->server,
                $this->getToken(),
                $email
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Inserts a new member
     * @param $email
     * @param array $dynContent
     * @return mixed
     */
    public function insertMember($email, array $dynContent)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/insertMember/%s",
                $this->server,
                $this->getToken()
            );

            $data = '<?xml version="1.0" encoding="utf-8"?><synchroMember>';
            if (!empty($dynContent)) {
                $data .= '<dynContent>';
                foreach ($dynContent as $key => $value) {
                    $data .= '<entry><key>' . $key . '</key><value>' . $value . '</value></entry>';
                }
                $data .= '</dynContent>';
            }
            $data .= '<email>' . $email . '</email></synchroMember>';

            $response = $this->cURL($url, 'POST', $data, false, array(
                    CURLOPT_HTTPHEADER => array('Content-Type:application/xml', 'Accept:application/xml'))
            );
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Updates one or multiple field for members identified by one or multiple criteria (FIRSTNAME:John|LASTNAME:Smith|EMAIL:johnsmith@smartfocus.com)
     * @param array $memberUID
     * @param array $dynContent
     * @return mixed
     */
    public function updateMember(array $memberUID, array $dynContent)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/updateMember/%s",
                $this->server,
                $this->getToken()
            );

            $data = '<?xml version="1.0" encoding="utf-8"?><synchroMember>';

            if (empty($memberUID)) {
                throw new \Exception('Cannot update member: No identifiers.');
            }
            $identifier = [];
            foreach ($memberUID as $key => $value) {
                $identifier[] = $key . ':' . $value;
            }
            $data .= '<memberUID>' . implode('|', $identifier) . '</memberUID>';

            if (!empty($dynContent)) {
                $data .= '<dynContent>';
                foreach ($dynContent as $key => $value) {
                    $data .= '<entry><key>' . $key . '</key><value>' . $value . '</value></entry>';
                }
                $data .= '</dynContent></synchroMember>';
            } else {
                throw new \Exception('Cannot update member: No value to update');
            }

            $response = $this->cURL($url, 'POST', $data, false, array(
                    CURLOPT_HTTPHEADER => array('Content-Type:application/xml', 'Accept:application/xml'))
            );
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Updates a single field for a member identified by its email
     * @param $email
     * @param $field
     * @param $value
     * @return mixed
     */
    public function updateMemberByEmailAddress($email, $field, $value)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/update/%s/%s/%s/%s",
                $this->server,
                $this->getToken(),
                $email,
                $field,
                $value
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Creates or updates one or multiple field for members identified by one or multiple criteria (FIRSTNAME:John|LASTNAME:Smith|EMAIL:johnsmith@smartfocus.com)
     * @param array $memberUID
     * @param array $dynContent
     * @return mixed
     */
    public function insertOrUpdateMember(array $memberUID, array $dynContent)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/insertOrUpdateMember/%s",
                $this->server,
                $this->getToken()
            );

            $data = '<?xml version="1.0" encoding="utf-8"?><synchroMember>';

            if (empty($memberUID)) {
                throw new \Exception('Cannot update member: No identifiers.');
            }
            $identifier = [];
            foreach ($memberUID as $key => $value) {
                $identifier[] = $key . ':' . $value;
            }
            $data .= '<memberUID>' . implode('|', $identifier) . '</memberUID>';

            if (!empty($dynContent)) {
                $data .= '<dynContent>';
                foreach ($dynContent as $key => $value) {
                    $data .= '<entry><key>' . $key . '</key><value>' . $value . '</value></entry>';
                }
                $data .= '</dynContent></synchroMember>';
            } else {
                throw new \Exception('Cannot update member: No value to update');
            }

            $response = $this->cURL($url, 'POST', $data, false, array(
                    CURLOPT_HTTPHEADER => array('Content-Type:application/xml', 'Accept:application/xml'))
            );
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the job status (i.e. the status of the member insertion or update) using the job ID
     * @param $jobId
     * @return mixed
     */
    public function getMemberJobStatus($jobId)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/getMemberJobStatus/%s/%s",
                $this->server,
                $this->getToken(),
                $jobId
            );

            $response = $this->cURL($url);
            if (isset($response['processStatus']) && isset($response['processStatus']['status'])) {
                $status = $response['processStatus']['status'];
                $status .= isset($response['processStatus']['email']) ? (': ' . $response['processStatus']['email']) : '';
                return $status;
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves a list of members who have the given email address
     * @param $email
     * @return mixed
     */
    public function getMembersByEmail($email)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/getMemberByEmail/%s/%s",
                $this->server,
                $this->getToken(),
                $email
            );
            $response = $this->cURL($url);
            if (isset($response['members'])) {
                return $this->formatMembers($response['members']);
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves a list of members who have the given cellphone
     * @param $cellphone
     * @return mixed
     */
    public function getMembersByCellphone($cellphone)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/getMemberByCellphone/%s/%s",
                $this->server,
                $this->getToken(),
                $cellphone
            );
            $response = $this->cURL($url);
            if (isset($response['members'])) {
                return $this->formatMembers($response['members']);
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves a member who has the given ID
     * @param $id
     * @return mixed
     */
    public function getMemberById($id)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/getMemberById/%s/%s",
                $this->server,
                $this->getToken(),
                $id
            );
            $response = $this->cURL($url);
            if (isset($response['members'])) {
                return $this->formatMembers($response['members']);

            }
            throw new \Exception(sprintf('No member found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves all members page by page. Each page contains 10 members
     * @param $page
     * @return mixed
     */
    public function getListMembersByPage($page)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/getListMembersByPage/%s/%s",
                $this->server,
                $this->getToken(),
                $page
            );
            $response = $this->cURL($url);

            if (isset($response['result'])) {
                $page = $response['result'];
                if (isset($page['list'])) {
                    $page['list'] = $this->formatMembers($page['list']);
                }
                return $page;
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves a list of members who match the given criteria.
     * @param array $memberUID
     * @return mixed
     */
    public function getMembers(array $memberUID)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/getMembers/%s",
                $this->server,
                $this->getToken()
            );

            if (empty($memberUID)) {
                throw new \Exception('Cannot update member: No identifiers.');
            }
            $identifier = [];
            foreach ($memberUID as $key => $value) {
                $identifier[] = $key . ':' . $value;
            }
            $data = '<?xml version="1.0" encoding="utf-8"?>' .
                '<synchroMember>' .
                '<memberUID>' . implode('|', $identifier) . '</memberUID>' .
                '</synchroMember>';

            $response = $this->cURL($url, 'POST', $data, false, array(
                    CURLOPT_HTTPHEADER => array('Content-Type:application/xml', 'Accept:application/xml'))
            );
            if (isset($response['members'])) {
                return $this->formatMembers($response['members']);
            }
            throw new \Exception(sprintf('No result found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Retrieves the list of fields (i.e. database column names) available in the Member table.
     * @return mixed
     */
    public function descMemberTable()
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/descMemberTable/%s",
                $this->server,
                $this->getToken()
            );

            $response = $this->cURL($url);
            if (isset($response['memberTable']) && isset($response['memberTable']['fields']) && isset($response['memberTable']['fields']['entry'])) {
                $fields = [];
                foreach ($response['memberTable']['fields']['entry'] as $field) {
                    $fields[$field['key']] = $field['value'];
                }
                return $fields;
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Unsubscribes the member(s) matching the given email address.
     * @param $email
     * @return mixed
     */
    public function unjoinByEmail($email)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/unjoinByEmail/%s/%s",
                $this->server,
                $this->getToken(),
                $email
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Unsubscribes the member(s) matching the given cellphone.
     * @param $cellphone
     * @return mixed
     */
    public function unjoinByCellphone($cellphone)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/unjoinByCellphone/%s/%s",
                $this->server,
                $this->getToken(),
                $cellphone
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Unsubscribes the member matching the given member ID.
     * @param $id
     * @return mixed
     */
    public function unjoinById($id)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/unjoinByMemberId/%s/%s",
                $this->server,
                $this->getToken(),
                $id
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Unsubscribes the member(s) matching the given criteria
     * @param array $memberUID
     * @return mixed
     */
    public function unjoinMember(array $memberUID)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/unjoinMember/%s",
                $this->server,
                $this->getToken()
            );

            if (empty($memberUID)) {
                throw new \Exception('Cannot update member: No identifiers.');
            }
            $identifier = [];
            foreach ($memberUID as $key => $value) {
                $identifier[] = $key . ':' . $value;
            }
            $data = '<?xml version="1.0" encoding="utf-8"?>' .
                '<synchroMember>' .
                '<memberUID>' . implode('|', $identifier) . '</memberUID>' .
                '</synchroMember>';

            $response = $this->cURL($url, 'POST', $data, false, array(
                    CURLOPT_HTTPHEADER => array('Content-Type:application/xml', 'Accept:application/xml'))
            );

            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Resubscribes the member(s) matching the given email address.
     * @param $email
     * @return mixed
     */
    public function rejoinByEmail($email)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/rejoinByEmail/%s/%s",
                $this->server,
                $this->getToken(),
                $email
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Resubscribes the member(s) matching the given cellphone
     * @param $cellphone
     * @return mixed
     */
    public function rejoinByCellphone($cellphone)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/rejoinByCellphone/%s/%s",
                $this->server,
                $this->getToken(),
                $cellphone
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Resubscribes the member matching the given ID.
     * @param $id
     * @return mixed
     */
    public function rejoinById($id)
    {
        try {
            $url = sprintf("https://%s/apimember/services/rest/member/rejoinByMemberId/%s/%s",
                $this->server,
                $this->getToken(),
                $id
            );

            $response = $this->cURL($url);
            if (isset($response['result'])) {
                return $response['result'];
            }
            throw new \Exception(sprintf('No status found in %s. (Error occured on %s)', serialize($response), $url));
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    ///////////////////// NOT INTERFACE METHODS /////////////////////

    /**
     * @param $url
     * @param string $method
     * @param null $data
     * @param bool $httpbuildquery
     * @param array $additionalOpts
     * @return array
     * @throws \Exception
     */
    protected function cURL($url, $method = 'GET', $data = null, $httpbuildquery = true, $additionalOpts = array())
    {
        $curl = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => strtoupper($method),
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true,
        );

        // multipart/form data post/put should be the only cases where we don't use http_build_query
        if (($method === 'POST' || $method === 'PUT') && !is_null($data)) {
            if ($httpbuildquery) {
                $data = http_build_query($data);
                $options[CURLOPT_HTTPHEADER] = array("Content-type: application/x-www-form-urlencoded");
            } else {
                $options[CURLOPT_HTTPHEADER] = array("Content-type: multipart/form-data");
            }
            $options[CURLOPT_POSTFIELDS] = $data;
        }


        if (!empty($additionalOpts)) {
            foreach ($additionalOpts as $opt => $value) {
                $options[$opt] = $value;
            }
        }
//        $options[CURLOPT_PROXY] = '127.0.0.1:8888';

        curl_setopt_array($curl, $options);

        $return = curl_exec($curl);
        curl_close($curl);

        if (!$return) {
            throw new \Exception(sprintf('404 Not Found (Error occured while %sing on %s)', $method, $url));
        }

        return $this->xmlResponseToArray($return, $url);
    }

    /**
     * Useful for multipart put. @see methods invoking this to understand.
     * @param $url
     * @param $params
     * @return string
     */
    protected function streamAndSend($url, $params)
    {
        $stream = stream_context_create($params);
        return @file_get_contents($url, FILE_TEXT, $stream);
    }

    /**
     * @param $file
     * @return string
     */
    protected function getChunk64FileContent($file)
    {
        return chunk_split(base64_encode(file_get_contents($file)));
    }


    /**
     * @param $response
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    protected function xmlResponseToArray($response, $url)
    {
        $xml = @simplexml_load_string($response);
        if ($xml === false) {
            throw new \Exception(sprintf('Unable to parse response for %s: %s', $url, $response));
        }
        $status = isset($xml['responseStatus']) ? (string)$xml['responseStatus'] : 'failed';
        if ($status != 'success') {
            /** @noinspection PhpUndefinedFieldInspection */
            $description = (string)$xml->description;
            if ($description != '') {
                $error = '';
                /** @noinspection PhpUndefinedFieldInspection */
                $status = (string)$xml->status;
                if ($status != '') {
                    $error .= '[' . $status . '] ';
                }
                $error .= $description;
            } else {
                $error = $response;
            }
            throw new \Exception(sprintf('%s (Error occured while parsing xml response for %s)', $error, $url));
        }

        // Then return everything as array
        $json = json_encode($xml);
        $array = json_decode($json, true);
        if (isset($array['@attributes'])) {
            unset($array['@attributes']);
        }
        return $array;
    }

    /**
     * @param $file
     * @param array $headers
     * @return string
     */
    protected function computeInsertUploadXml($file, array $headers)
    {
        // Compute the xml itself
        $xml = sprintf('<?xml version="1.0" encoding="UTF-8"?><insertUpload>
                        <fileName>%s</fileName>
                        <fileEncoding>UTF-8</fileEncoding>
                        <separator>%s</separator>
                        <dateFormat>mm/dd/yyyy</dateFormat>
                        <mapping>', basename($file), CsvFileGenerator::CSV_SEPARATOR);

        // Col + 1 car on liste les champs à partir de 1 au lieu de 0 dans le xml
        foreach ($headers as $col => $key) {
            $xml .= sprintf('<column><colNum>%s</colNum><fieldName>%s</fieldName></column>', $col + 1, $key);
        }

        $xml .= '</mapping></insertUpload>';

        return $xml;
    }

    /**
     * @param $file
     * @param array $headers
     * @param array $mergeCriteria
     * @return string
     */
    protected function computeMergeUploadXml($file, array $headers, array $mergeCriteria)
    {
        // First compute merge criteria
        $criteria = '';
        foreach ($mergeCriteria as $mergeCriterion) {
            $criteria .= sprintf('<criteria>%s</criteria>', $mergeCriterion);
        }

        // Compute the xml itself
        $xml = sprintf('<?xml version="1.0" encoding="UTF-8"?><mergeUpload>
                        <fileName>%s</fileName>
                        <fileEncoding>UTF-8</fileEncoding>
                        <separator>%s</separator>
                        %s
                        <dateFormat>mm/dd/yyyy</dateFormat>
                        <mapping>', basename($file), CsvFileGenerator::CSV_SEPARATOR, $criteria);

        $sfHeaders = array_keys($headers);
        // Col + 1 car on liste les champs à partir de 1 au lieu de 0 dans le xml
        foreach ($sfHeaders as $col => $key) {
            $xml .= '<column>';
            $xml .= sprintf('<colNum>%s</colNum><fieldName>%s</fieldName>', $col + 1, $key);
            // If $col is a criterion, set <toReplace> to false. else true.
            $xml .= '<toReplace>' . (in_array($col, $mergeCriteria) ? 'false' : 'true') . '</toReplace>';
            $xml .= '</column>';
        }

        $xml .= '</mapping></mergeUpload>';

        return $xml;
    }

    /**
     * @param array $dirtyMembers
     * @param bool $singleResult
     * @return array
     */
    protected function formatMembers(array $dirtyMembers, $singleResult = false)
    {
        $members = [];
        foreach ($dirtyMembers as $member) {
            // Single member found
            if (isset($member['attributes'])) {
                $item = [];
                foreach ($member['attributes'] as $attribute) {
                    foreach ($attribute as $entry) {
                        $item[$entry['key']] = isset($entry['value']) ? $entry['value'] : null;
                    }
                }
                ksort($item);
                $members[] = $item;
            } else { // Multiple members found
                $members = $this->formatMembers($member);
            }
        }
        return $members;
    }
}