<?php

namespace La\SmartFocusMembersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ExportController
 * @package La\SmartFocusMembersBundle\Controller
 */
class ExportController extends Controller
{

    public function debugAction(Request $request, $jobId)
    {
        /** @var \La\SmartFocusMembersBundle\Model\Client\RestClient $client */
        $client = $this->get('la_smart_focus_members.client.rest');
        echo '<pre>';
        echo $client->getMemberJobStatus($jobId);
        die();
    }

    public function descAction(Request $request)
    {
        $client = $this->get('la_smart_focus_members.client.rest');
        die(var_export($client->descMemberTable(), true));
    }


    public function summaryAction(Request $request)
    {
        $client = $this->get('la_smart_focus_members.client.rest');
        var_dump($client->getUploadSummaryList(1, 10, 'date', array('status' => 'done', 'source' => 'api_batch_member')));
        die();
    }


    public function monitorAction(Request $request)
    {
        $client = $this->get('la_smart_focus_members.client.rest');
        echo "<table><thead><th>Id</th><th>Source</th><th>ShortStatus</th><th>Status</th></thead>";
        $lastUploads = $client->getLastUploads();
        foreach ($lastUploads as $upload) {
            echo "<tr><td>" . $upload['id'] . "</td><td>" . $upload['source'] . "</td><td>" . $upload['status'] . "</td><td>" . $client->getUploadStatus($upload['id']) . "</td></tr>";
        }

        echo "</table>";
        die();
    }

    public function demoAction(Request $request)
    {
        /** @var \La\SmartFocusMembersBundle\Model\Client\RestClient $client */
        $client = $this->get('la_smart_focus_members.client.rest');
//        $return = $client->updateMember(array('EMAIL' => 'spinosaa@smartfocus.fr', 'FIRSTNAME' => 'toto'), array('FIRSTNAME' => 'tutu', 'LASTNAME' => 'tete'));
//        $return = $client->updateMemberByEmailAddress('gigi@smartfocus.fr','FIRSTNAME','tututu');
        $return = $client->insertOrUpdateMember(array('EMAIL' => 'gigi@smartfocus.fr'), array('FIRSTNAME' => 'so glad we', 'LASTNAME' => 'almost made it'));
//        $return = $client->getListMembersByPage('1');
//        $return = $client->getMembers(array('EMAIL' => 'spinosa@smartfocus.fr'));
//        $return = $client->descMemberTable();
//        $return = $client->unjoinMember(array('EMAIL' => 'spinosa@smartfocus.fr'));
        $return = $client->getMemberJobStatus($return);

        var_dump($return);
        die();
    }
}