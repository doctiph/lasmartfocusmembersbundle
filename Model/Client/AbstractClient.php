<?php

namespace La\SmartFocusMembersBundle\Model\Client;

/**
 * Class AbstractClient
 * @package La\SmartFocusMembersBundle\Model\Client
 */

abstract class AbstractClient implements ClientInterface
{
    /**
     * The server on which request to SmartFocus are sent
     * @var string $server
     */
    protected $server;

    /**
     * SmartFocus session token
     * @var string
     */
    protected $token = null;

    /**
     * SmartFocus session token expiration date
     * @var int|null
     */
    protected $tokenExpires = null;

    /**
     * @param $server
     * @param $login
     * @param $password
     * @param $key
     */
    public function __construct($server, $login, $password, $key)
    {
        $this->server = $server;
        $this->login = $login;
        $this->password = $password;
        $this->key = $key;

        $this->token = $this->getToken();
    }

    /**
     * @param $token
     */
    protected function setToken($token)
    {
        $this->token = $token;
        $this->tokenExpires = time() + static::SMARTFOCUS_SESSION_TOKEN_MAX_AGE;
    }

    /**
     * If token is null or expired, reopen a session
     * @return string
     */
    protected function getToken()
    {
        if ($this->token === null || $this->tokenIsExpired()) {
            $this->open($this->login, $this->password, $this->key);
        }
        return $this->token;
    }

    /**
     * Close a session and unset the token
     */
    protected function unsetToken()
    {
        try {
            if ($this->token != null) {
                $this->close();
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        $this->token = null;
        $this->tokenExpires = null;
    }

    /**
     * Clean handling of exceptions
     * @param \Exception $e
     * @throws \Exception
     */
    protected function handleException(\Exception $e)
    {
        $this->unsetToken();
        throw $e;
    }

    /**
     * @return bool
     */
    protected function tokenIsExpired()
    {
        return time() >= $this->tokenExpires;
    }

    /**
     * Close and
     */
    public function __destruct()
    {
        $this->unsetToken();
    }

}