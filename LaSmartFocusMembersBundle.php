<?php

namespace La\SmartFocusMembersBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use La\SmartFocusMembersBundle\DependencyInjection\CompilerPass\ProviderCompilerPass;

class LaSmartFocusMembersBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ProviderCompilerPass());
    }
}
