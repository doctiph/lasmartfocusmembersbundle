<?php
namespace La\SmartFocusMembersBundle\Model\Provider;

/**
 * Interface MemberProviderInterface
 * @package La\SmartFocusMembersBundle\Model\Provider
 */
interface MemberProviderInterface
{

    /**
     * @param \DateTime $since
     * @param null $offset
     * @param null $limit
     * @return array
     */
    public function getMembers(\DateTime $since, $offset = null, $limit = null);

    /**
     * @param $user
     * @return mixed
     */
    public function buildMember($user);

    /**
     * Get a fake user
     * @return mixed
     */
    public function getStubUser();

    /**
     * @param array $mapping
     */
    public function setMapping($mapping);

    /**
     * @return array
     */
    public function getMapping();

    /**
     * @return mixed
     */
    public function clear();

    /**
     * @param $property
     * @return mixed
     */
    public function validateProviderGetter($mappingField);

}