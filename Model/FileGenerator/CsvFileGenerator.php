<?php

namespace La\SmartFocusMembersBundle\Model\FileGenerator;

class CsvFileGenerator implements FileGeneratorInterface
{
    const CSV_SEPARATOR = ';';

    /**
     * @var array
     */
    protected $mapping = array();
    /**
     * @var array
     */
    protected $header = array();

    /**
     * @param $kernelRootDir
     */
    public function __construct($kernelRootDir)
    {
        $this->kernelRootDir = $kernelRootDir;
    }

    /**
     * @param array $mapping
     * @param bool $withHeader
     * @return \SplFileObject
     * @throws \Exception
     */
    public function createFile(array $mapping, $withHeader = false)
    {
        try {
            $this->mapping = $mapping;
            $file = new \SplFileObject($this->getFileName(), 'w+');
            if ($withHeader) {
                $this->insertHeader($file);
            }
            return $file;
        } catch (\Exception $e) {
            throw new \Exception(sprintf('An error occured while generating CSV file: %s', $e->getMessage()));
        }
    }

    /**
     * @param array $mapping
     * @return array
     * @throws \Exception
     */
    public function getHeader(array $mapping = null)
    {
        if (empty($this->mapping)) {
            if (is_null($mapping)) {
                throw new \Exception('No mapping set, cannot get header for ' . $this->getFileName());
            }
            $this->mapping = $mapping;
        }
        return array_keys($this->mapping);
    }

    /**
     * Get file header
     * @param \SplFileObject $file
     * @param array $mapping
     * @return mixed
     * @throws \Exception
     */
    public function insertHeader(\SplFileObject $file, array $mapping = null)
    {
        $file->fputcsv($this->getHeader($mapping), static::CSV_SEPARATOR);
    }

    /**
     * Insert line. If a new file is needed to insert the line, returns the new file. Instead returns null.
     * @param \SplFileObject $file
     * @param array $line
     * @param array $mapping
     * @param bool $withHeader
     * @return null|\SplFileObject
     * @throws \Exception
     */
    public function insertLine(\SplFileObject $file, array $line, array $mapping = null, $withHeader = false)
    {
        try {
            $maxFileSize = static::MAX_FILE_SIZE * 1000000; // o to Mo

            $cleanLine = array();
            foreach ($this->getHeader($mapping) as $key) {
                $cleanLine[$key] = isset($line[$key]) ? $line[$key] : '';
            }
            $lineSize = $file->fputcsv($cleanLine, static::CSV_SEPARATOR);
            if (!$lineSize) {
                throw new \Exception('An error occured while inserting ' . @serialize($cleanLine));
            }
            $fsize = $file->fstat()[7];
            if ($fsize >= $maxFileSize) {
                $file->ftruncate($fsize - $lineSize);
                $newFile = $this->createFile($mapping, $withHeader);
                $newFile->fputcsv($cleanLine, static::CSV_SEPARATOR);
                return $newFile;
            }
            return null;
        } catch (\Exception $e) {
            throw new \Exception(sprintf('An error occured while generating CSV file: %s', $e->getMessage()));
        }
    }

    /**
     * Get file name
     * @return mixed
     */
    public function getFileName()
    {
        $path = $this->kernelRootDir . '/export';
        if (!is_dir($path)) {
            mkdir($path);
        }
        $date = new \DateTime();
        $tmpName = $path . '/members_' . $date->format('Y_m_d_H_i_s') . '.csv';
        $filename = $tmpName;
        $i = 1;
        while (file_exists($filename)) {
            $filename = $tmpName . '_' . $i++;
        }
        return $filename;
    }

    /**
     * Delete current file
     * @param $file
     * @return mixed|void
     * @throws \Exception
     */
    public function deleteFile($file)
    {
        @unlink($file);
        if (file_exists($file)) {
            throw new \Exception('Unsuccessful deleting ' . $this->getFileName());
        }
    }

    /**
     * @param \Exception $e
     * @throws \Exception
     */
    protected function handleException(\Exception $e)
    {
//        $this->deleteFile();
        throw $e;
    }
}
