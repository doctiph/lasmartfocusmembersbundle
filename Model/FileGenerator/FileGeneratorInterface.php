<?php

namespace La\SmartFocusMembersBundle\Model\FileGenerator;

interface FileGeneratorInterface
{
    /** Over this size (Mo), split it and queue multiple files */
    const MAX_FILE_SIZE = 256;
//    const MAX_FILE_SIZE = 0.0005;

    /**
     * Create a new file
     * @param array $mapping
     * @param $withHeader
     * @return mixed
     */
    public function createFile(array $mapping, $withHeader = false);

    /**
     * Insert file header
     * @param \SplFileObject $file
     * @return mixed
     */
    public function insertHeader( \SplFileObject $file);


    /**
     * Get file header
     * @return array
     * @throws \Exception
     */
    public function getHeader();

    /**
     * Insert a line in file
     * @param \SplFileObject $file
     * @param array $line
     * @param array $mapping
     * @param bool $withHeader
     * @return mixed
     */
    public function insertLine(\SplFileObject $file, array $line, array $mapping = null, $withHeader = false);

    /**
     * Get file name
     * @return mixed
     */
    public function getFileName();

    /**
     * @param $file
     * @return mixed
     */
    public function deleteFile($file);

}