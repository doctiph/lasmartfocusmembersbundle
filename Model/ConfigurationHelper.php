<?php
namespace La\SmartFocusMembersBundle\Model;

use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigurationManager
 * @package La\SmartFocusMembersBundle\Model
 */
class ConfigurationHelper
{
    const CONFIG_KEY = 'la_smart_focus_members';
    const CONFIG_FILENAME = 'smartfocus_members.yml';
    const ADD_MONOLOG_CONFIG = true;

    /**
     * @var string
     */
    protected $appDir;

    /**
     * @param $appDir
     */
    public function __construct($appDir)
    {
        $this->appDir = $appDir;
    }

    /**
     * @return string
     */
    public function getConfigFile($filename = null)
    {
        if ($filename === null) {
            $filename = static::CONFIG_FILENAME;
        }
        return sprintf('%s/config/%s', $this->appDir, $filename);
    }

    /**
     * @return array
     */
    public function loadConfig()
    {
        // Get file or create it.
        $configFile = $this->getConfigFile();
        if (!file_exists($configFile)) {
            file_put_contents($configFile, '');
        }

        // Load file
        $config = Yaml::parse(file_get_contents($configFile));
        if (is_array($config) && isset($config[static::CONFIG_KEY])) {
            $config = $config[static::CONFIG_KEY];
        } else {
            $config = [];
        }
        return $config;
    }

    public function addMonologConfiguration()
    {
        if (static::ADD_MONOLOG_CONFIG) {
            $this->writeConfig($this->getMonologConfiguration(), static::CONFIG_FILENAME, true);
        }
    }

    public function hasMonologConfiguration()
    {
        $file = $this->getConfigFile();
        $config = Yaml::parse(file_get_contents($file));
        return is_array($config) && count(array_diff_key($this->getMonologConfiguration(), $config)) > 0;
    }

    public function getMonologConfiguration()
    {
        return array('monolog' =>
            array('handlers' =>
                array('smartfocus_members' =>
                    array(
                        'type' => 'stream',
                        'path' => '%kernel.logs_dir%/smartfocus_members.log',
                        'channels' => 'smartfocus_members'
                    )
                )
            )
        );
    }

    /**
     * @return bool
     */
    public function isConfigImported($key = 'imports')
    {
        $appConfig = Yaml::parse(file_get_contents($this->getConfigFile('config.yml')));
        $importedConfigs = [];
        if (isset($appConfig['imports'])) {
            foreach ($appConfig['imports'] as $import) {
                $importedConfigs[] = $import['resource'];
            }
        }
        return in_array(static::CONFIG_FILENAME, $importedConfigs);
    }

    /**
     * @param array $configuration
     * @param array $ignoredFields
     * @return bool
     */
    public function isFullyConfigured(array $configuration = null, array $ignoredFields = [])
    {
        if ($configuration === null) {
            $configuration = $this->loadConfig();
        }

        if (static::ADD_MONOLOG_CONFIG && $this->hasMonologConfiguration()) {
            $this->addMonologConfiguration();
        }

        // Validate configuration keys
        $missingKeys = array_diff_key($this->getConfigurationStructure(), $configuration);
        // Ignore fields if necessary
        if (count($ignoredFields)) {
            foreach ($ignoredFields as $ignoredField) {
                if (isset($missingKeys[$ignoredField])) {
                    unset($missingKeys[$ignoredField]);
                }
            }
        }
        // if no key is missing
        if (empty($missingKeys)) {
            // Validate configuration values types
            $validValueType = true;
            foreach ($this->getConfigurationStructure() as $key => $type) {
                // ignore fields if necessary
                if (in_array($key, $ignoredFields)) {
                    continue;
                }
                $valueType = is_array($type) ? is_array($configuration[$key]) : !is_array($configuration[$key]);
                $validValueType &= $valueType;
            }
            return $validValueType;
        }
        return false;
    }

    /**
     * @param $key
     * @param array $config
     * @return bool
     */
    public function isValidNode($key, array $config)
    {
        $structure = $this->getConfigurationStructure();
        // valid key
        if (!isset($config[$key])) {
            return false;
        }
        // valid type
        return is_array($structure[$key]) ? is_array($config[$key]) : !is_array($config[$key]);
    }

    /**
     * @return array
     */
    public function getConfigurationStructure()
    {
        return array(
            'server' => 'scalar',
            'login' => 'scalar',
            'password' => 'scalar',
            'key' => 'scalar',
            'mapping' => array()
        );
    }

    /**
     * @param array $configuration
     * @param bool $append
     */
    public function writeConfig(array $configuration, $filename = null, $append = false)
    {
        if (is_null($filename)) {
            $filename = static::CONFIG_FILENAME;
        }
        $yaml = Yaml::dump($configuration, 10);
        $append ? file_put_contents($this->getConfigFile($filename), PHP_EOL . PHP_EOL . $yaml, FILE_APPEND) : file_put_contents($this->getConfigFile($filename), $yaml);
    }

    /**
     * Validate actual mapping structure based on curl on client.
     * @param array $mapping
     */
    public function validateMappingStructure(array $mapping)
    {

    }

    /**
     * @param Client\ClientInterface $client
     */
    public function getRemoteMappingStructure(ClientInterface $client)
    {
        $structure = $client->descMemberTable();
        die(var_dump($structure));
    }

}