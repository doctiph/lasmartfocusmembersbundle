<?php
namespace La\SmartFocusMembersBundle\Command;

use La\SmartFocusMembersBundle\Model\Provider\MemberProviderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use La\SmartFocusMembersBundle\Model\ConfigurationHelper;

/**
 *
 * php app/console smartfocus:config:generate
 */
class GenerateConfigCommand extends ContainerAwareCommand
{
    protected $structure;

    public function __construct($name = null)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('smartfocus:config:generate')
            ->setDescription('Generate bundle configuration for current application');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $echo = new CommandFormater($output, $input, $this->getApplication(), $this->getContainer()->getParameter('smartfocus.members.enabled'));
        $echo->isBundleActive();

        try {
            $echo->skipLine();
            $echo->ln('*** This command will guide you through LaSmartFocusMembersBundle configuration. ***');
            $echo->skipLine();

            /** @var ConfigurationHelper $helper */
            $helper = $this->getContainer()->get('la_smart_focus_members.configuration_helper');

            $config = $helper->loadConfig();

            $choices = array(
                0 => 'Generate config structure',
                1 => 'Generate mapping structure (SmartFocus Credentials must be valid)',
                2 => 'Validate smartfocus fields (SmartFocus Credentials must be valid)',
                3 => 'Validate local getters',
                4 => 'Exit'

            );

            $choice = $echo->choice('Choose a task to execute:', $choices);

            switch ($choice) {
                case $choices[0]:
                    // ignore mapping for now (configured in option 1)
                    $ignored = array('mapping');

                    if (!$helper->isFullyConfigured(null, $ignored)) {
                        $echo->ln('Please enter the following config values (or let them blank to change them manually) :');
                        $structure = $helper->getConfigurationStructure();
                        foreach ($structure as $key => $type) {
                            if (in_array($key, $ignored)) {
                                continue;
                            }
                            if (!$helper->isValidNode($key, $config)) {
                                $config[$key] = $echo->prompt(ucfirst($key) . ':', null);
                            }
                        }
                        $config = array($helper::CONFIG_KEY => $config);

                        $helper->writeConfig($config);
                        $echo->skipLine();
                        $echo->ok('The following configuration has been written in <success>' . $helper::CONFIG_FILENAME . '</success>:');
                        $echo->yaml(array($config));
                        $echo->skipLine();
                    }
                    $echo->ok('The config structure is valid. To manually change the configuration values, please check ' . $helper::CONFIG_FILENAME);
                    if (!$helper->isConfigImported()) {
                        $echo->ko('The config file ' . $helper::CONFIG_FILENAME . ' is not properly loaded. Please add it in your imports in config.yml');
                    }
                    break;
                case $choices[1]:
                    try {
                        /** @var ClientInterface $client */
                        $client = $this->getContainer()->get('la_smart_focus_members.client.rest');
                        $config = $helper->loadConfig();

                        do {
                            $validProvider = true;
                            $provider = $echo->prompt('Please enter the alias of the provider to configure (Default: lauser): ', 'lauser');
                            if (isset($config['mapping'][$provider]) && !empty($config['mapping'][$provider])) {
                                $validProvider = $echo->confirm('The provider ' . $provider . ' seems to be already configured. Overwrite it ? Y/N (default: yes)');
                            }
                        } while ($validProvider === false);

                        $config['mapping'][$provider] = array_fill_keys(array_keys($client->descMemberTable()), null);
                        $helper->writeConfig(array($helper::CONFIG_KEY => $config));
                        $echo->ok('Successfully generated mapping structure for ' . $provider . ': Please complete your mapping in ' . $helper::CONFIG_FILENAME);
                    } catch (\Exception $e) {
                        if ($helper->isFullyConfigured()) {
                            if (!$helper->isConfigImported()) {
                                $echo->ko('The config file ' . $helper::CONFIG_FILENAME . ' is not properly loaded. Please add it in your imports in config.yml');
                            } else {
                                $echo->ko('The bundle was fully configured but the values are not correct. To manually change them, check ' . $helper::CONFIG_FILENAME . '.');
                            }
                        } else {
                            $echo->ko('The bundle was not fully configured. Please generate configuration before generating mapping structure');
                        }
                    }
                    break;
                case$choices[2]:
                    try {
                        $client = $this->getContainer()->get('la_smart_focus_members.client.rest');
                        $config = $helper->loadConfig();
                        if (!isset($config['mapping']) || !is_array($config['mapping'])) {
                            throw new \Exception('Not fully configured');
                        }

                        $validFields = array_keys($client->descMemberTable());
                        foreach ($config['mapping'] as $provider => $fields) {
                            foreach ($fields as $field => $value) {
                                if (!in_array($field, $validFields)) {
                                    $echo->ko('Invalid field: ' . $field . ' for provider ' . $provider . '.');
                                }
                            }
                            $echo->ok('Finished validating mapping structure for provider ' . $provider . '.');
                        }
                    } catch (\Exception $e) {
                        if ($helper->isFullyConfigured()) {
                            if (!$helper->isConfigImported()) {
                                $echo->ko('The config file ' . $helper::CONFIG_FILENAME . ' is not properly loaded. Please add it in your imports in config.yml');
                            } else {
                                $echo->ko('The bundle was fully configured but the values are not correct. To manually change them, check ' . $helper::CONFIG_FILENAME . '.');
                            }
                        } else {
                            $echo->ko('The bundle was not fully configured. Please generate configuration before validating mapping structure');
                        }
                    }
                    break;
                case $choices[3]:
                    try {
                        $config = $helper->loadConfig();
                        if (!isset($config['mapping']) || !is_array($config['mapping'])) {
                            throw new \Exception('Not fully configured');
                        }
                        if (empty($config['mapping'])) {
                            throw new \Exception('At least one provider must be configured to run this option.');
                        }
                        $configuredProviders = array_keys($config['mapping']);
                        $aliasChoice = $echo->choice('Choose a provider to validate:', $configuredProviders);
                        $alias = $configuredProviders[$aliasChoice];

                        if ($this->getContainer()->has('la_smart_focus_members.provider.' . $alias)) {
                            /** @var MemberProviderInterface $provider */
                            $provider = $this->getContainer()->get('la_smart_focus_members.provider.' . $alias);
                            $rows = [];
                            foreach ($config['mapping'][$alias] as $field => $value) {
                                if (is_null($value)) {
                                    $rows[] = array(
                                        'NULL',
                                        $field,
                                        'NULL',
                                        'No mapping: ignored.'
                                    );
                                } else {
                                    $valid = $provider->validateProviderGetter($value);
                                    $rows[] = array(
                                        $valid ? '<success> OK </success>' : '<error> KO </error>',
                                        $valid ? '<success> ' . $field . ' </success>' : '<error> ' . $field . '</error>',
                                        $valid ? '<success> ' . $value . ' </success>' : '<error> ' . $value . '</error>',
                                        $valid ? '<success> ' . $valid . ' </success>' : '<error> Invalid Mapping </error>',
                                    );
                                }
                            }
                            $echo->table(array('Result', 'SmartFocus Field', 'Local mapping', 'Comment'), $rows);
                        } else {
                            throw new \Exception('The provider ' . $alias . ' does not exist, is not fully configured or is not registered as "la_smart_focus_members.provider.' . $alias . '". Aborting.');
                        }
                    } catch (\Exception $e) {
                        $echo->ko($e->getMessage());
                    }
                    break;
                case $choices[4]:
                default:
                    break;
            }

        } catch (\Exception $e) {
            $echo->handleException($e);
        }
    }


}