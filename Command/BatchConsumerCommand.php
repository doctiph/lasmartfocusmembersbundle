<?php
namespace La\SmartFocusMembersBundle\Command;

use La\SmartFocusMembersBundle\Model\Client\ClientInterface;
use La\SmartFocusMembersBundle\Model\Provider\MemberProviderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use La\SmartFocusMembersBundle\Model\Queue\FileQueue;

/**
 *
 * php app/console smartfocus:batch:export {rest (default)| soap}

 */
class BatchConsumerCommand extends ContainerAwareCommand
{
    protected $fileBeingConsumed = null;
    protected $providerAlias = null;

    protected function configure()
    {
        $this
            ->setName('smartfocus:batch:consumer')
            ->setDescription('Consume queue an export batch file to SmartFocus "mass data" webservice.')
            ->addArgument(
                'webservice',
                InputArgument::OPTIONAL,
                'Using REST (default) or SOAP webservice',
                'REST'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $echo = new CommandFormater($output, $input, $this->getApplication(), $this->getContainer()->getParameter('smartfocus.members.enabled'));
        $echo->isBundleActive();

        try {
            $type = $input->getArgument('webservice');
            $echo->title('Consuming queue to export to SmartFocus using ' . $type . ' webservice');

            $queueDir = $this->getContainer()->getParameter('kernel.root_dir') . '/export';
            $queue = new FileQueue($queueDir);

            if ($queue->count() === 0) {
                throw new \Exception('Nothing to consume.');
            }

            /** @var ClientInterface $client */
            $client = $this->getContainer()->get('la_smart_focus_members.client.' . strtolower($type));
            $echo->ok('Established connection to SmartFocus.');

            while ($queue->getAvailableUploadSlots($client) > 0) {
                if ($queue->count() === 0) {
                    $echo->ok('All files in queue have been processed. Exiting.');
                    exit();
                }

                /** New file being consumed */
                $this->fileBeingConsumed = null;
                $this->providerAlias = null;

                $echo->li(sprintf('%d upload slots available.',$queue->getAvailableUploadSlots($client)));
                do {
                    $consumable = $queue->consume($client);
                    if (!isset($consumable['file']) || !isset($consumable['provider'])) {
                        $consumable = null;
                        $echo->ko('IGNORED - A corrupted file was found in queue.');
                    } else if (!file_exists($consumable['file'])) {
                        $echo->ko('IGNORED - The following file was found in queue but doesn\'t exist: ' . $consumable['file']);
                    }
                } while (!file_exists($consumable['file']) || $consumable == null);
                $file = $consumable['file'];

                $providerAlias = $consumable['provider'];
                if (is_null($providerAlias) || !$this->getContainer()->has('la_smart_focus_members.provider.' . $providerAlias)) {
                    throw new \Exception('Invalid provider: ' . $providerAlias . '. No valid provider registered under this alias. Error occured while dequeueing file: ' . $file);
                }

                /** Save to requeue them if something goes wrong */
                $this->fileBeingConsumed = $file;
                $this->providerAlias = $providerAlias;

                /** @var MemberProviderInterface $provider */
                $provider = $this->getContainer()->get('la_smart_focus_members.provider.' . $providerAlias);
                $uploadId = $client->uploadAndMerge($file, $provider->getMapping());
                $echo->ok('The file '.$file.' was sent with id: ' . $uploadId);
                @unlink($file);
                if (file_exists($file)) {
                    throw new \Exception('Unsuccessful deleting ' . $file);
                }
            }
            $echo->ok('No available slots to upload. Exiting.');
        } catch (\Exception $e) {
            try {
                if (!is_null($this->fileBeingConsumed) && !is_null($this->providerAlias)) {
                    $queueDir = $this->getContainer()->getParameter('kernel.root_dir') . '/export';
                    $queue = new FileQueue($queueDir);
                    $queue->queue($this->fileBeingConsumed, $this->providerAlias);
                }
            } catch (\Exception $e) {
                $echo->handleException($e);
            }
            $echo->handleException($e);
        }
    }

}