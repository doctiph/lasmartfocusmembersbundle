<?php
namespace La\SmartFocusMembersBundle\Model\Queue;

use La\SmartFocusMembersBundle\Model\Client\ClientInterface;

/**
 * Interface QueueInterface
 * @package La\SmartFocusMembersBundle\Model\Queue
 */
interface QueueInterface
{

    /**
     * Adds a file to the queue
     * @param $file
     * @param $providerAlias
     * @return mixed
     */
    public function queue($file, $providerAlias);

    /**
     * Updates a queue (caution: overwrites whole queue)
     * @param $queue
     * @return mixed
     */
    public function updateQueue($queue);


    /**
     * Erases a queue (caution: overwrites whole queue)
     * @return mixed
     */
    public function eraseQueue();

    /**
     * Returns count of currently queued items
     * @return mixed
     */
    public function count();

    /**
     * Consumes the queue (FIFO) with given $client
     * @param ClientInterface $client
     * @return array
     */
    public function consume(ClientInterface $client);

    /**
     * Gets available upload slots to consume queue
     * @param ClientInterface $client
     * @return int
     */
    public function getAvailableUploadSlots(ClientInterface $client);

}