<?php

namespace La\SmartFocusMembersBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    const DEFAULT_CONFIGURATION_VALUE = null;

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_smart_focus_members');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enabled')->defaultFalse()->end()
                ->scalarNode('server')->defaultValue(static::DEFAULT_CONFIGURATION_VALUE)->end()
                ->scalarNode('login')->defaultValue(static::DEFAULT_CONFIGURATION_VALUE)->end()
                ->scalarNode('password')->defaultValue(static::DEFAULT_CONFIGURATION_VALUE)->end()
                ->scalarNode('key')->defaultValue(static::DEFAULT_CONFIGURATION_VALUE)->end()
                ->arrayNode('widget')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('search_result')->defaultNull()->end()
                        ->scalarNode('no_result')->defaultNull()->end()
                    ->end()
                ->end()
                ->arrayNode('mapping')
                    ->useAttributeAsKey('provider')
                    ->prototype('array')
                        ->prototype('scalar')->defaultValue(static::DEFAULT_CONFIGURATION_VALUE)->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
