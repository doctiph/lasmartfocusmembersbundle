<?php
namespace La\SmartFocusMembersBundle\Model\Provider;

use Psr\Log\LoggerInterface;
use La\SmartFocusMembersBundle\Event\MemberUpdateEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AbstractMemberProvider
 * @package La\SmartFocusMembersBundle\Model\Provider
 */
abstract class AbstractMemberProvider implements MemberProviderInterface
{
    const EMAIL_SMARTFOCUS_FIELD = 'EMAIL';

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $mapping = array();

    /**
     * @var string
     */
    protected $alias;


    protected function __construct(EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }


    /**
     * @param $mapping
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;
    }

    /**
     * @return array
     */
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param \DateTime $since
     * @param null $offset
     * @param null $limit
     * @return array
     */
    abstract public function getMembers(\DateTime $since, $offset = null, $limit = null);

    /**
     * @return mixed
     */
    abstract public function getStubUser();

    /**
     * @param $user
     * @return array|null
     * @throws \Exception
     */
    public function buildMember($user)
    {
        if (empty($this->mapping)) {
            throw new \Exception('No mapping was defined for provider ' . get_class($this));
        }

        $member = [];

        // If user is not an object, transform it into a stdClass
        if (!is_object($user)) {
            if (is_array($user)) {
                $array = $user;
                $user = new \stdClass();
                foreach ($array as $key => $value) {
                    $user->key = $value;
                }
            } else {
                throw new \Exception('Cannot handle given user to build a member (Type: ' . get_class($user) . ')');
            }
        }
        $reflection = new \ReflectionClass($user);

        foreach ($this->mapping as $smartFocusField => $mappingField) {
            // user property: get property value through reflection.
            if ($reflection->hasProperty($mappingField)) {
                $reflectionProperty = $reflection->getProperty($mappingField);
                $reflectionProperty->setAccessible(true);
                $value = $reflectionProperty->getValue($user);
            } else {
                // factory d'un getter custom défini dans le provider (exemple: getAstrologicalSign($user) )
                $getter = 'get' . ucfirst($mappingField);
                if (method_exists($this, $getter)) {
                    $value = $this->$getter($user);
                } // else if no custom getter is set but the __get magic method returns a closure
                else if ($this->$mappingField instanceof \Closure) {
                    $getter = $this->$mappingField;
                    $value = $getter($user);
                } // else we don't know how to get the value: set it to null.
                else {
                    $value = null;
                }
            }

            // If an email is found, check for its validity. If not valid, log an error and ignore the member.
            if ($smartFocusField === static::EMAIL_SMARTFOCUS_FIELD) {
                if ($this->isValidEmail($value) === false) {
                    throw new \Exception('Invalid email: ' . $value);
                }
            }

            // clean value : format DateTime or sanitize strings
            if ($value instanceof \DateTime) {
                $value = $value->format('m/d/Y');
            } else if (is_string($value)) {
                $value = $this->sanitize($value);
            }
            $member[$smartFocusField] = $value;
        }

        return $member;
    }

    /**
     * Sanitize a string
     * @param $string
     * @return mixed
     */
    protected function sanitize($string)
    {
        $string = str_replace('"', '', $string); // '\"' instead of '"'?
        return $string;
    }

    /**
     * @todo
     * @param $email
     * @return bool
     */
    protected function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);
    }

    /**
     * Implemented because you may need to override it if you need to clear your provider from any stored data on a regular basis.
     */
    public function clear()
    {
    }

    /**
     * The method any event listener is expected to call when trying to update a member on smart focus
     *
     * @param $user
     */
    public function createAndSendMember($user)
    {
        try {
            $member = $this->buildMember($user);
            $memberEvent = new MemberUpdateEvent($member);
            $this->dispatcher->dispatch($memberEvent::SMARTFOCUS_MEMBER_UPDATE, $memberEvent);
        } catch (\Exception $e) {
            $this->logger->error('Did not send member. Reason : ' . $e->getMessage());
        }
    }

    /**
     * @param $mappingField
     * @return bool|string
     */
    public function validateProviderGetter($mappingField)
    {
        $reflection = new \ReflectionClass($this->getStubUser());
        if ($reflection->hasProperty($mappingField)) {
            return 'User property';
        } else {
            $getter = 'get' . ucfirst($mappingField);
            if (method_exists($this, $getter)) {
                return 'Custom provider method';
            } // else if no custom getter is set but the __get magic method returns a closure
            else if ($this->$mappingField instanceof \Closure) {
                return 'Custom use of provider __get';
            } // else we don't know how to get the value
            else {
                return false;
            }
        }
    }
}