# Api Members Smartfocus

## Glossaire
  
  Dans cette documentation, les termes suivants seront utilisés:
  
  - __Member__: Un 'member' est un utilisateur unique sur SmartFocus. Un member est identifié par un ID et possède des champs CRM (ex: email, prénom, adresse, etc.) 
    
  - __Provider__: Un provider est une classe PHP qui sait transformer un utilisateur venant de votre système de gestion d'utilisateurs en un member SmartFocus.  Chaque provider a donc un mapping spécifique pour faire correspondre chaque propriété du member à une propriété de l'utilisateur du site. (Voir la rubrique dédiée aux providers de member plus bas)  
    
## Installation

### composer.json

      "require": {
        ....
        "la/smartfocusmembersbundle": "~1.0.0"
      },

    -> composer update      

### AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            ...
            new La\SmartFocusMembersBundle\LaSmartFocusMembersBundle()
        );
        ...
    }
    
### Configuration

  /!\ La configuration du LaSmartFocusMembersBundle nécessite que vous ayez en votre possession vos informations de connexion à SmartFocus
  
  Commencez par lancer la commande suivante: 

    php app/console smartfocus:config:generate
    
  Différentes options sont disponibles. Choisissez __Generate config structure__.  
  Remplissez vos informations de connexion au fur et à mesure que la commande vous les demande (Vous pouvez aussi les copier-coller plus tard dans le fichier généré)   
    
  Une fois que la commande est terminée, le fichier de configuration est généré: Vous pouvez le modifier manuellement ou relancer la commande pour remplir des paramètres que vous n'aviez pas renseigné.  
    
  Ajoutez le fichier de configuration aux imports de config.yml:
   
   _config.yml_
   
       imports:
           ...
           - { resource: smartfocus_members.yml }
    
  Si vos informations de connexion à smartfocus sont correctement renseignées, vous pouvez continuer et générer le mapping pour votre provider.
  Avant de continuer, veuillez lire la section __Provider__ ci-dessous pour choisir le provider que vous utiliserez.
  Une fois que vous connaissez l'alias de votre provider, re-lancez la commande:  
    
    php app/console smartfocus:config:generate
                                               
  Cette fois, choisissez l'option  __Generate mapping structure__.  
  La commande va se connecter à SmartFocus pour vous générer un "squelette" de mapping pour votre provider.  
  Vous avez juste à remplacer les valeurs "null" pour chaque champ SmartFocus qui vous intéresse et à le remplacer par une propriété ou une méthode de votre provider. (Cf. la section __Provider__ ci-dessous)
  
  Pour finir, activez le bundle dans la configuration d'environnement qui vous intéresse, par exemple:
  
  _config_prod.yml_
  
    la_smart_focus_members:
        enabled: true
  
## Utilisation
  
  Le bundle LaSmartFocusMembersBundle propose deux type de mise à jour / insertion de members: individuelle ou collective (batch).
  Afin de garantir l'intégrité des données il est recommendé de mettre en place les deux systèmes de communication.
  
### Individual Member Api
  
  L'insertion et la mise à jour individuelle des members permet une synchronisation en continu et garantit des données à jour en permanence.
    
  A chaque fois que vous souhaitez envoyer une mise à jour / insertion individuelle, dispatchez l'évènement connu par votre provider:
  
          /* Récupérer l'évènement qui contient votre user */
          $memberEvent = $this->provider->getMemberEvent($user);
          
          /* Dispatcher l'évènement */
          $this->dispatcher->dispatch($memberEvent->getMemberUpdateEvent(), $memberEvent);
  
### Batch Member Api  

  L'insertion et la mise à jour collective (en batch) des members permet de garantir qu'aucune information ne s'est perdue lors de la communication individuelle présentée ci-dessus.  
  En règle générale, on effectue un seul envoi quotidien en batch, qui revalide toutes les communications individuelles de la journée.  
    
  Pour mettre en place cette synchronisation quotidienne, mettez en place les crons suivants sur votre serveur:
  
        # Génération des fichiers
        php app/console smartfocus:batch:generate alias_de_votre_provider
        # Envoi des fichiers
        php app/console smartfocus:batch:consumer

## Annexe: Provider

  Un provider est une classe PHP capable de transforme en member un utilisateur issu de votre système de gestion des utilisateurs.
  Un provider est identifié par son __alias__, spécifié dans son tag 'la_smart_focus_members.member_provider' lors de sa définition en service:
  
              <tag name="la_smart_focus_members.member_provider" alias="alias_du_provider"/>

### Si vous utilisez le LaUserBundle (version 1.3.9 et plus)

  Si vous utilisez le LaUserBundle (version 1.3.9 et plus), le bundle possède un provider natif.  
  Son alias est __lauser__.   
    
  Dans votre mapping, vous pouvez 
  
#### Si vous utilisez un autre système de gestion d'utilisateur

  If you want to use another one, you'll have to create a provider extending the abstract user provider with your own logic.

  Create a service extending La/SmartFocusMembersBundle/Model/Provider/AbstractUserProvider    
  It musts implements the method getMembers($date, $offset, $limit). This method provides an array of members (or an instance of \Iterator)    
  It will also implements any custom getter you need.  
  
  Define in your configuration that you'll be using your service as user provider by changing the 'provider' node value under la_smart_focus_members:  
  
        la_smart_focus_members:  
            ....  
            provider: your_custom.user_provider # (That's your service name)  
  
  Note: Make sure that your user provider is able to handle all the fields you defined in your SmartFocus mapping, or they'll be ignored.
 
## Annexe: Configuration Reference
 
     la_smart_focus_members:
         server:               ~ # Required
         login:                ~ # Required
         password:             ~ # Required
         key:                  ~ # Required
         providers:              # Required
             # Prototype
             name:
                 mapping:     [] # Required

