<?php

namespace La\SmartFocusMembersBundle\Tests\Model\Client;

use La\SmartFocusMembersBundle\Model\Client\RestClient;

/**
 * @runTestsInSeparateProcesses
 */
class RestClientTest extends \PHPUnit_Framework_TestCase
{
    protected $server = 'emv.emv.com';
    protected $login = 'login';
    protected $password = 'password';
    protected $key = 'key';
    protected $mapping = array('EMAIL' => 'email', 'FIRST_NAME' => 'firstName');
    protected $token = 'TOKEN1234';

//    public function setUp(){}
//    public function tearDown(){}

    /* Tests */

    /**
     * @group MassAPI
     * @covers RestClient::open
     */
    public function testOpen()
    {
        $client = $this->buildClient();

        $client->expects($this->once())->method('cURL')->will($this->returnValue(array('result' => $this->token)));

        $client->__construct($this->server, $this->login, $this->password, $this->key);

        $this->assertEquals($this->getValue($client, 'token'), $this->token);
        $this->assertFalse($this->invoke($client, 'tokenIsExpired'));
    }

    /**
     * @group MassAPI
     * @covers RestClient::open
     */
    public function testClose()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will($this->returnValue(array('result' => $this->token)));
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $client->close();
        $this->assertNull($this->getValue($client, 'token'));
        $this->assertTrue($this->invoke($client, 'tokenIsExpired'));
    }

    /**
     * @group MassAPI
     * @covers RestClient::handleException
     */
    public function testCloseOnException()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will($this->returnValue(array('result' => $this->token)));
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);


        try {
            $exception = new \Exception('test');
            $this->invoke($client, 'handleException', $exception);
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), 'Exception');
        }

        $this->assertNull($this->getValue($client, 'token'));
        $this->assertTrue($this->invoke($client, 'tokenIsExpired'));
    }

    /**
     * @group MassAPI
     * @covers RestClient::uploadAndInsert
     */
    public function testUploadAndInsert()
    {
        $file = 'file.csv';

        $additionalMethods = array(
            'getChunk64FileContent' => array(
                'expects' => $this->once(),
                'with' => $file,
                'will' => $this->returnValue(chunk_split(base64_encode('MICHEL;DEBUG')))
            ),
            'streamAndSend' => array(
                'expects' => $this->once(),
                'will' => $this->returnValue(
                    '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
                     <response responseStatus="success"> <result>1</result></response>')
            ),
        );

        $client = $this->buildClient($additionalMethods);
        $client->expects($this->once())->method('cURL')->will($this->returnValue(array('result' => $this->token)));
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $id = $client->uploadAndInsert($file, array('FIRSTNAME', 'LASTNAME'));

        $this->assertEquals($id, 1);
    }

    /**
     * @group MassAPI
     * @covers RestClient::uploadAndMerge
     */
    public function testUploadAndMerge()
    {
        $file = 'file.csv';

        $additionalMethods = array(
            'getChunk64FileContent' => array(
                'expects' => $this->once(),
                'with' => $file,
                'will' => $this->returnValue(chunk_split(base64_encode('MICHEL;DEBUG')))
            ),
            'streamAndSend' => array(
                'expects' => $this->once(),
                'will' => $this->returnValue(
                    '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
                     <response responseStatus="success"> <result>1</result></response>')
            ),
        );

        $client = $this->buildClient($additionalMethods);
        $client->expects($this->once())->method('cURL')->will($this->returnValue(array('result' => $this->token)));
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $id = $client->uploadAndMerge($file, array('FIRSTNAME', 'LASTNAME'));

        $this->assertEquals($id, 1);
    }

    /**
     * @group MassAPI
     * @covers RestClient::getLastUploads
     */
    public function testGetLastUploads()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('lastUploads' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        // returns expected on success
        $this->assertEquals($client->getLastUploads(), 'test');
    }

    /**
     * @group MassAPI
     * @covers RestClient::getImportQueueStatus
     */
    public function testGetImportQueueStatus()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getImportQueueStatus(), 'test');
    }

    /**
     * @group MassAPI
     * @covers RestClient::getUploadSummaryList
     */
    public function testGetUploadSummaryList()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('lastUploads' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getUploadSummaryList(), 'test');
    }

    /**
     * @group MassAPI
     * @covers RestClient::getUploadStatus
     */
    public function testGetUploadStatus()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('uploadStatus' => array('status' => 'TEST', 'details' => 'OK'))
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getUploadStatus('test'), 'TEST: OK');
    }

    /**
     * @group MassAPI
     * @covers RestClient::getLogFile
     */
    public function testGetLogFile()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getLogFile('test'), 'test');
    }

    /**
     * @group MassAPI
     * @covers RestClient::getBadFile
     */
    public function testGetBadFile()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getBadFile('test'), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::insertMemberByEmailAddress
     */
    public function testInsertMemberByEmailAddress()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->insertMemberByEmailAddress('test'), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::insertMember
     */
    public function testInsertMember()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->insertMember('test', array('test')), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::updateMember
     */
    public function testUpdateMember()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->updateMember(array('email' => 'test'), array('test')), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::updateMemberByEmailAddress
     */
    public function testUpdateMemberByEmailAddress()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->updateMemberByEmailAddress('test', 'email', 'test'), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::insertOrUpdateMember
     */
    public function testInsertOrUpdateMember()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'test')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->insertOrUpdateMember(array('email' => 'test'), array('test')), 'test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getMemberJobStatus
     */
    public function testGetMemberJobStatus()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('processStatus' => array('status' => 'TEST', 'email' => 'test'))
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getMemberJobStatus('test'), 'TEST: test');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getMembersByEmail
     */
    public function testGetMembersByEmail()
    {
        $members = array('members' => array(
            'member' => array(
                'attributes' => array(
                    array('entry' =>
                        array('key' => 'FIRSTNAME', 'value' => 'toto')
                    ),
                    array('entry' =>
                        array('key' => 'LASTNAME', 'value' => 'test')
                    ),
                    array('entry' =>
                        array('key' => 'BIRTHDATE')
                    ),
                )
            )
        ));

        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                $members
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getMembersByEmail('test'), array(array('FIRSTNAME' => 'toto', 'LASTNAME' => 'test', 'BIRTHDATE' => null)));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getMembersByCellphone
     */
    public function testGetMembersByCellphone()
    {
        $members = array('members' => array(
            'member' => array(
                'attributes' => array(
                    array('entry' =>
                        array('key' => 'FIRSTNAME', 'value' => 'toto')
                    ),
                    array('entry' =>
                        array('key' => 'LASTNAME', 'value' => 'test')
                    ),
                    array('entry' =>
                        array('key' => 'BIRTHDATE')
                    ),
                )
            )
        ));

        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                $members
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getMembersByCellphone('0627581258'), array(array('FIRSTNAME' => 'toto', 'LASTNAME' => 'test', 'BIRTHDATE' => null)));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getMembersById
     */
    public function testGetMembersById()
    {
        $members = array('members' => array(
            'member' => array(
                'attributes' => array(
                    array('entry' =>
                        array('key' => 'FIRSTNAME', 'value' => 'toto')
                    ),
                    array('entry' =>
                        array('key' => 'LASTNAME', 'value' => 'test')
                    ),
                    array('entry' =>
                        array('key' => 'BIRTHDATE')
                    ),
                )
            )
        ));

        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                $members
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getMemberById(666), array(array('FIRSTNAME' => 'toto', 'LASTNAME' => 'test', 'BIRTHDATE' => null)));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getListMembersByPage
     */
    public function testGetListMembersByPage()
    {
        //@todo
        $this->assertTrue(true);

//        $members = array(
//            'currentPage' => 1,
//            'list' => array(
//                array(
//                'attributes' => array(
//                    array('entry' =>
//                        array('key' => 'FIRSTNAME', 'value' => 'toto')
//                    ),
//                    array('entry' =>
//                        array('key' => 'LASTNAME', 'value' => 'test')
//                    ),
//                    array('entry' =>
//                        array('key' => 'BIRTHDATE')
//                    ),
//                )),
//                array(
//                'attributes' => array(
//                    array('entry' =>
//                        array('key' => 'FIRSTNAME', 'value' => 'tata')
//                    ),
//                    array('entry' =>
//                        array('key' => 'LASTNAME', 'value' => 'test')
//                    ),
//                    array('entry' =>
//                        array('key' => 'BIRTHDATE')
//                    ),
//                )),
//        ));
//
//        $client = $this->buildClient();
//        $client->expects($this->exactly(2))->method('cURL')->will(
//            $this->onConsecutiveCalls(
//                array('result' => $this->token),
//                array('result' => $members)
//            )
//        );
//        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);
//
//        $this->assertEquals($client->getListMembersByPage(1), array(array('FIRSTNAME' => 'toto', 'LASTNAME' => 'test', 'BIRTHDATE' => null), array('FIRSTNAME' => 'tata', 'LASTNAME' => 'test', 'BIRTHDATE' => null)));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::getMembers
     */
    public function testGetMembers()
    {
        $members = array('members' => array(
            'member' => array(
                'attributes' => array(
                    array('entry' =>
                        array('key' => 'FIRSTNAME', 'value' => 'toto')
                    ),
                    array('entry' =>
                        array('key' => 'LASTNAME', 'value' => 'test')
                    ),
                    array('entry' =>
                        array('key' => 'BIRTHDATE')
                    ),
                )
            )
        ));

        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                $members
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->getMembers(array('EMAIL' => 'test', 'FIRSTNAME' => 'toto')), array(array('FIRSTNAME' => 'toto', 'LASTNAME' => 'test', 'BIRTHDATE' => null)));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::descMemberTable
     */
    public function testDescMemberTable()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('memberTable' => array('fields' => array('entry' => array(
                    array('key' => 'FIRSTNAME', 'value' => 'STRING'),
                    array('key' => 'LASTNAME', 'value' => 'STRING'),
                    array('key' => 'BIRTHDATE', 'value' => 'DATE'),
                ))))
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->descMemberTable(), array('FIRSTNAME' => 'STRING', 'LASTNAME' => 'STRING', 'BIRTHDATE' => 'DATE'));
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::unjoinByEmail
     */
    public function testUnjoinByEmail()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->unjoinByEmail('test'), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::unjoinByCellphone
     */
    public function testUnjoinByCellphone()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->unjoinByCellphone('test'), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::unjoinById
     */
    public function testUnjoinById()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->unjoinById('test'), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::unjoinMember
     */
    public function testUnjoinMember()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->unjoinMember(array('EMAIL'=>'test')), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::rejoinByEmail
     */
    public function testRejoinByEmail()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->rejoinByEmail('test'), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::rejoinByCellphone
     */
    public function testRejoinByCellphone()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->rejoinByCellphone('test'), 'success');
    }

    /**
     * @group IndividualAPI
     * @covers RestClient::rejoinById
     */
    public function testRejoinById()
    {
        $client = $this->buildClient();
        $client->expects($this->exactly(2))->method('cURL')->will(
            $this->onConsecutiveCalls(
                array('result' => $this->token),
                array('result' => 'success')
            )
        );
        $client->__construct($this->server, $this->login, $this->password, $this->key, $this->mapping);

        $this->assertEquals($client->rejoinById('test'), 'success');
    }

    ///////////////////////////////////////// TEST UTILS //////////////////////////////////////////////


    /**
     * @param array $additionalMethods
     * @return RestClient
     */
    protected function buildClient(array $additionalMethods = array())
    {
        $methods = array_merge(
            array('cURL'),
            array_keys($additionalMethods)
        );
        $stub = $this->getMockBuilder('La\SmartFocusMembersBundle\Model\Client\RestClient')
            ->setMockClassName('RestClient')
            ->setMethods($methods)
            ->disableOriginalConstructor()
            ->getMock();

        foreach ($additionalMethods as $method => $behaviour) {
            $stub->expects(isset($behaviour['expects']) ? $behaviour['expects'] : $this->any())
                ->method($method)
                ->with(isset($behaviour['with']) ? $behaviour['with'] : $this->anything())
                ->will(isset($behaviour['will']) ? $behaviour['will'] : $this->returnValue(true));
        }
        return $stub;
    }

    /**
     * @param RestClient $client
     * @param $method
     * @param null $params
     * @return mixed
     * @throws \Exception
     */
    protected function invoke(RestClient $client, $method, $params = null)
    {
        try {
            $reflection = new \ReflectionMethod('La\SmartFocusMembersBundle\Model\Client\RestClient', $method);
            $reflection->setAccessible(true);

            return $reflection->invoke($client, $params);
        } catch (\Exception $e) {
            throw new \Exception(sprintf('Tried to invoke %s on %s but got: %s', $method, get_class($client), $e->getMessage()));
        }
    }

    /**
     * @param RestClient $client
     * @param $property
     * @return mixed
     * @throws \Exception
     */
    protected function getValue(RestClient $client, $property)
    {
        try {
            $reflection = new \ReflectionProperty('La\SmartFocusMembersBundle\Model\Client\RestClient', $property);
            $reflection->setAccessible(true);
            return $reflection->getValue($client);
        } catch (\Exception $e) {
            throw new \Exception(sprintf('Tried to get Value of %s on %s but got: %s', $property, get_class($client), $e->getMessage()));
        }
    }

}

