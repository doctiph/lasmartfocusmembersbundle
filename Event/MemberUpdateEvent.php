<?php

namespace La\SmartFocusMembersBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class MemberUpdateEvent
 * @package La\SmartFocusMembersBundle\Event
 */
class MemberUpdateEvent extends Event
{
    /**
     * The SMARTFOCUS_MEMBER_UPDATE event occurs whenever a request to update a single member is sent
     * The expected data is the member created from your application user by your member provider
     * The event listener method receives a MemberUpdateEvent instance.
     */
    const SMARTFOCUS_MEMBER_UPDATE = 'smartfocus.member.update';


    protected $member;

    protected $jobId;

    /**
     * @param $member
     */
    public function __construct($member)
    {
        $this->member = $member;
        $this->jobId = null;
    }

    /**
     * @return null
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * @param null $jobId
     */
    public function setJobId($jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * @return mixed
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param mixed $member
     */
    public function setMember($member)
    {
        $this->member = $member;
    }
}