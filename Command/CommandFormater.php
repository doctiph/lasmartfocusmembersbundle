<?php
namespace La\SmartFocusMembersBundle\Command;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class CommandFormater
{
    /** @var OutputInterface */
    protected $output;
    /** @var InputInterface* */
    protected $input;
    /** @var  Application */
    protected $application;
    /** @var bool */
    protected $debug;
    /** @var bool */
    protected $active;

    public function __construct(OutputInterface $output, InputInterface $input, Application $application, $active, $debug = false)
    {
        $this->debug = $debug;
        $this->active = $active;
        $this->application = $application;
        $this->output = $output;
        $this->input = $input;

        $styles = array(
            'title' => array('white', 'magenta', array('bold')),
            'success' => array('white', 'green', array('bold')),
            'warning' => array('black', 'yellow', array('bold')),
            'debug' => array('red', null, array('bold')),
            'li' => array('magenta', null, array('bold')),
            'yaml' => array('yellow', null, array('bold')),
        );
        foreach ($styles as $name => $style) {
            $ofs = new OutputFormatterStyle($style[0], $style[1], $style[2]);
            $output->getFormatter()->setStyle($name, $ofs);
        }
    }

    public function isBundleActive()
    {
        if ($this->active === false) {
            $this->ko('Error: The bundle is not active. (The configuration <debug>la_smart_focus_members.enabled</debug> is set to <debug>false</debug>.)');
            exit;
        }
    }

    public function send($msg)
    {
        $this->output->write($msg);
    }

    public function ln($msg)
    {
        $this->output->writeln($msg);
    }

    public function skipLine()
    {
        $this->ln('');
    }

    public function title($msg)
    {
        $this->output->writeln('<title>[' . $msg . ']</title>');
    }

    public function ok($msg)
    {
        $this->output->writeln('<success>[OK]</success> ' . $msg);
    }

    public function ko($msg)
    {
        $this->output->writeln('<error>[KO]</error> ' . $msg);
    }

    public function warning($msg)
    {
        $this->output->writeln('<warning>[WARNING]</warning> ' . $msg);
    }

    public function li($msg)
    {
        $this->output->writeln('    <li>* ' . $msg . '</li>');
    }

    public function yaml(array $array)
    {
        // If array was passed with root node as initial key, set the array to begin at root node, not '0';
        if(count($array) === 1 && isset($array[0])){
            $array = $array[0];
        }
        $smartHash = str_repeat('-', strlen(array_keys($array)[0]));
        $this->output->writeln('<yaml>'.$smartHash.'</yaml>');
        $this->output->write('<yaml>'.$this->toYaml($array).'</yaml>');
        $this->output->writeln('<yaml>'.$smartHash.'</yaml>');
    }

    protected function toYaml(array $array, $level = 0)
    {
        $yamlString = '';
        $indent = str_repeat('  ', $level);
        foreach($array as $key => $value){
            $yamlString .= sprintf('%s%s: ',$indent,$key);
            if(is_array($value)){
                $yamlString .= PHP_EOL . $this->toYaml($value, $level+1);
            }else{
                $yamlString .= $value . PHP_EOL;
            }
        }
        return $yamlString;
    }

    public function prompt($msg, $default = null)
    {
        /** @var Helper\QuestionHelper $helper */
        $helper = $this->application->getHelperSet()->get('question');
        $q = new Question\Question('<li>* ' . $msg . '</li> ', $default);
        return $helper->ask($this->input, $this->output, $q);
    }

    public function confirm($msg, $default = true)
    {
        /** @var Helper\QuestionHelper $helper */
        $helper = $this->application->getHelperSet()->get('question');
        $q = new Question\ConfirmationQuestion('    <li>* ' . $msg . '</li> ', $default);
        return $helper->ask($this->input, $this->output, $q);
    }

    public function choice($msg, $options, $default = null, $attempts = null, $errorMessage = 'Invalid value: "%s". Pick again.')
    {
        if (isset($options[$default])) {
            if (in_array(substr($msg, -1), array(':', '?', ' ', '.'))) {
                $msg = substr($msg, -1);
            }
            $msg = $msg . ' (DEFAULT: ' . $options[$default] . ')';
        }

        /** @var Helper\QuestionHelper $helper */
        $helper = $this->application->getHelperSet()->get('question');
        $q = new Question\ChoiceQuestion('<warning>' . $msg . '</warning> ', $options, $default);
        $q->setMaxAttempts($attempts);
        $q->setErrorMessage($errorMessage);
        $selected = $helper->ask($this->input, $this->output, $q);


        $this->skipLine();
        $this->title($selected);
        $this->skipLine();
        return $selected;
    }

    public function table(array $headers, array $rows)
    {
        $table = new Helper\Table($this->output);
        $table->setHeaders($headers);
        $table->setRows($rows);
        $table->render();
    }

    public function debug($msg)
    {
        $this->output->writeln('<debug>=> ' . $msg . '</debug>');
    }

    public function memory()
    {
        if ($this->debug) {
            $this->debug(sprintf("Memory usage: %.2f MB", (memory_get_usage() / (1024 * 1024))));
        }
    }

    public function is_verbose()
    {
        $output = $this->output;
        return $output->getVerbosity() > $output::VERBOSITY_NORMAL;
    }

    public function handleException(\Exception $e)
    {
        if ($e instanceof ServiceNotFoundException) {
            $this->output->writeln('<error>[KO]</error> An error occured while requesting the service ' . $e->getId() . '.');
        } else {
            $this->output->writeln('<error>[KO]</error> ' . $e->getMessage());
        }
        exit();
    }

}