<?php

namespace La\SmartFocusMembersBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LaSmartFocusMembersExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('smartfocus.members.configuration', $config);

        // la_smart_focus_members.enabled
        $container->setParameter('smartfocus.members.enabled', $config['enabled']);


        $container->setParameter('smartfocus.members.server', $config['server']);
        $container->setParameter('smartfocus.members.login', $config['login']);
        $container->setParameter('smartfocus.members.password', $config['password']);
        $container->setParameter('smartfocus.members.key', $config['key']);
        $container->setParameter('smartfocus.members.mapping', $config['mapping']);

        foreach ($config['widget'] as $key => $widget) {
            $container->setParameter('smartfocus.members.widget.' . $key, $widget);
        }

        //Load services only if the bundle is enabled
        if ($config['enabled']) {
            $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
            $loader->load('services.yml');
        }
    }

}
