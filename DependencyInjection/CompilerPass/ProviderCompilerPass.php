<?php

namespace La\SmartFocusMembersBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ProviderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $mapping = $container->getParameter('smartfocus.members.mapping');

        $available_member_providers = [];

        $taggedServices = $container->findTaggedServiceIds(
            'la_smart_focus_members.member_provider'
        );

        foreach ($taggedServices as $id => $tags) {

            $definition = $container->getDefinition($id); // new Reference($id),

            if (!isset($tags[0]) || !isset($tags[0]['alias'])) {
                throw new \Exception('The service ' . $id . ' was tagged as a member provider but doesn\'t have an alias.');
            }
            $alias = $tags[0]['alias'];
            $definition->addMethodCall('setAlias', array($alias));

            if (isset($mapping[$alias])) {
                $definition->addMethodCall(
                    'setMapping',
                    array($mapping[$alias])
                );

                // Add to available providers
                $available_member_providers[$alias] = $id;
            }
        }

        // Available providers
        $container->setParameter('la_smart_focus_members.available_member_providers', $available_member_providers);

    }
}