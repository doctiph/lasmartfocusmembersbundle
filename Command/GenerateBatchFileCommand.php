<?php
namespace La\SmartFocusMembersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use La\SmartFocusMembersBundle\Model\Queue\FileQueue;
use La\SmartFocusMembersBundle\Model\FileGenerator\FileGeneratorInterface;
use La\SmartFocusMembersBundle\Model\Provider\MemberProviderInterface;

/**
 *
 * php app/console smartfocus:batch:generate
 */
class GenerateBatchFileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('smartfocus:batch:generate')
            ->setDescription('Generate batch file for SmartFocus "mass update" webservice')
            ->addArgument(
                'provider',
                InputArgument::REQUIRED,
                'Provider alias used to compute members for SmartFocus'
            )
            ->addOption(
                'since',
                null,
                InputOption::VALUE_OPTIONAL,
                'Number of days to export',
                1 // 7
            )->addOption(
                'batch',
                null,
                InputOption::VALUE_OPTIONAL,
                'Number of users to process at a time',
                1000
            )->addOption(
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Debug mode: display more informations'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $echo = new CommandFormater($output, $input, $this->getApplication(), $this->getContainer()->getParameter('smartfocus.members.enabled'), $input->getOption('debug'));
        $echo->isBundleActive();

        try {
            $providerAlias = $input->getArgument('provider');
            if (is_null($providerAlias) || !$this->getContainer()->has('la_smart_focus_members.provider.' . $providerAlias)) {
                throw new \Exception('Invalid provider: ' . $providerAlias . '. No valid provider registered under this alias.');
            }
            /** @var MemberProviderInterface $provider */
            $provider = $this->getContainer()->get('la_smart_focus_members.provider.' . $providerAlias);
            /** @var FileGeneratorInterface $fileGenerator */
            $fileGenerator = $this->getContainer()->get('la_smart_focus_members.file_generator.csv');

            $since = $input->getOption('since');
            if (!is_numeric($since)) {
                throw new \Exception('The \'since\' option must be a valid integer.');
            }

            $date = new \DateTime();
            $date->modify(sprintf('- %d days', $since));

            $batch = $input->getOption('batch');
            if (!is_numeric($batch)) {
                throw new \Exception('The \'batch\' option must be a valid integer.');
            }

            $echo->title('Generating batch export file to smartfocus');

            // Create file
            $files = [];
            $file = $fileGenerator->createFile($provider->getMapping());

            $offset = 0;
            $limit = $batch;
            $found = false;

            /** @var \Iterator $iterateMembers */
            while ($iterateMembers = $provider->getMembers($date, $offset, $batch)) {
                $echo->memory();
                if (!$iterateMembers instanceof \Iterator) {
                    if (is_array($iterateMembers)) {
                        $iterateMembers = new \ArrayIterator((array)$iterateMembers);
                    } else {
                        throw new \Exception('The user provider must return an array or an instance of \Iterator.');
                    }
                }
                // Start iteration to check if it's valid
                $iterateMembers->next();

                if ($iterateMembers->valid() === false) {
                    break;
                }
                $found = true;

                $echo->ok(sprintf('Creating up to %d members [offset: %d, limit: %d] and inserting:', $batch, $offset, $limit));
                $errors = [];

                if ($echo->is_verbose()) {
                    $progress = new ProgressBar($output);
                    $progress->start();
                }

                /** @var \Iterator $iterateMembers */
                while ($iterateMembers->valid()) {

                    $iterateMember = $iterateMembers->current();
                    if ($echo->is_verbose()) {
                        $progress->advance();
                    }
                    $member = is_array($iterateMember) && isset($iterateMember[0]) ? $iterateMember[0] : $iterateMember;
                    try {
                        $built = $provider->buildMember($member);
                        $newFile = $fileGenerator->insertLine($file, $built, $provider->getMapping());

                        // If insert line doesnt return null, it returns a new file: save first one and continue with new file
                        if (!is_null($newFile) && $newFile instanceof \SplFileObject) {
                            $files[] = $file;
                            $file = $newFile;
                        }
                        $iterateMembers->next();

                        // clear
                        unset($member);
                        unset($built);
                        unset($iterateMember);
                    } catch (\Exception $e) {
                        $errors[] = sprintf('[%s] %s', $member->getEmail(), $e->getMessage());
                        $iterateMembers->next();
                    }
                }
                if ($echo->is_verbose()) {
                    $progress->finish();
                    $echo->skipLine();
                }

                foreach ($errors as $error) {
                    $echo->ko('FAILED: ' . $error);
                }
                $offset += $batch;
                $limit += $batch;
                $provider->clear();
            }

            if ($found === false) {
                throw new \Exception('No member found to export.');
            }
            $files[] = $file;

            $echo->ok('The following files were successfully generated:');
            /** @var \SplFileObject $file */
            foreach ($files as $file) {
                $echo->li(sprintf("%s (%.2f Ko)", $file->getFileName(), $file->fstat()[7] / 1024));
            }

            $echo->ok('Queueing files for export.');
            $queueDir = $this->getContainer()->getParameter('kernel.root_dir') . '/export';
            $queue = new FileQueue($queueDir);

            if ($echo->is_verbose()) {
                $progress = new ProgressBar($output);
                $progress->start(count($files));
            }
            foreach ($files as $file) {
                $queue->queue($file->getFileName(), $providerAlias);
                if ($echo->is_verbose()) {
                    $progress->advance();
                }
            }
            if ($echo->is_verbose()) {
                $progress->finish();
                $echo->skipLine();
            }
            $echo->ok('Queue successful: ' . $queue->count() . ' files in queue.');

        } catch (\Exception $e) {
            $echo->handleException($e);
        }
    }


}